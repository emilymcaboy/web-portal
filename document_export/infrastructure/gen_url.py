

def gen_url(base, final_param = '', **kwargs):

    
    url = "?"

    for key, value in kwargs.items():
        if value is not None and value != "":
            url += f"{key}={value}&"
    
    if url == '?':
        url = ''
    else:
        url += final_param

    if len(url) > 0:
        if url[-1] == '&':
            url = url[0:len(url) - 1]

    return f'{base}{url}'


