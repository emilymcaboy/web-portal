import flask
from werkzeug.datastructures import MultiDict

class RequestDictionary(dict):
    def __init__(self, *args, default_val=None, **kwargs):
        self.default_val = default_val
        super().__init__(*args, **kwargs)

        def __getattr__(self, key):
            return self.get(key, self.default_val)


def create(default_val=None, **route_args) -> RequestDictionary:
    request = flask.request

    args = request.args
    if isinstance(request.args, MultiDict):
        args = request.args.to_dict()

    form = request.form
    if isinstance(request.form, MultiDict):
        request.form.to_dict()

    data = {
        **args,
        **request.headers,
        **form,
        **route_args
    }

    return RequestDictionary(data, default_val=default_val)