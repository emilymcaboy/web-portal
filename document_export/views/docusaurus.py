from flask import session, Blueprint, request, send_file, redirect, g, flash
from services.get_files import get_file_names, download_file, zip_files, merge_pdfs
from services.get_combo_values import get_plants

from infrastructure.view_modifiers import response
from services.active_directory import ldap

from viewmodels.documents.tickets_viewmodel import TicketViewModel
from viewmodels.shared.viewmodelbase import ViewModelBase

blueprint = Blueprint("home", __name__, template_folder="templates")


@blueprint.route("/tickets", methods=["GET", "POST"])
@blueprint.route("/dm/tickets", methods=["GET", "POST"])
@response(template_file="shared/_filtertable.html")
@ldap.login_required
def tickets_test():

    vm = TicketViewModel()

    action, result = vm.process_request('MaterialTicket')

    if action in ['Download', 'Redirect']:
        return result

    result['plants'] = vm.plants()

    result['filters'] = vm.create_filters()

    return result

   


@blueprint.route("/", methods=["GET", "POST"])
@response(template_file="index.html")
def index():

    vm = ViewModelBase()
    return vm.to_dict()
