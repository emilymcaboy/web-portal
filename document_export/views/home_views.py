from types import MethodDescriptorType
from flask import session, Blueprint, request, send_file, redirect, g, flash, Markup
from flask.wrappers import Response
from services.get_combo_values import get_plants
import json

from infrastructure.view_modifiers import response
from services.active_directory import ldap
from services.get_featured_items import get_featured_items, get_text_elements
from services.get_files import download_files, zip_files

from viewmodels.documents.po_viewmodel import POViewModel
from viewmodels.documents.ap_invoice_viewmodel import APInvoiceViewModel
from viewmodels.documents.tickets_viewmodel import TicketViewModel
from viewmodels.shared.viewmodelbase import ViewModelBase
from viewmodels.documents.imaging_viewmodel import ImagingViewModel
from viewmodels.api.employee_images_viewmodel import EmployeeImagesViewmodel
from viewmodels.employee_directory_viewmodel import EmployeeDirectoryViewmodel
from viewmodels.api.division_images_viewmodel import DivisionImagesViewmodel
from viewmodels.asphalt_projections_viewmodel import AsphaltProjectionsViewmodel
from viewmodels.asphalt_projections_v2 import AsphaltProjectionsV2
from viewmodels.docs.edit_doc_viewmodel import EditDocViewModel
from viewmodels.docs.get_doc_viewmodel import GetDocViewModel

blueprint = Blueprint("home", __name__, template_folder="templates")


@blueprint.route("/tickets", methods=["GET", "POST"])
@blueprint.route("/dm/tickets", methods=["GET", "POST"])
@response(template_file="shared/_filtertable.html")
@ldap.group_required(["_ERP-Users"])
def tickets_test():

    vm = TicketViewModel()

    action, result = vm.process_request("MaterialTicket")

    if action in ["Download", "Redirect"]:
        return result

    result["plants"] = vm.plants()

    result["filters"] = vm.create_filters()

    result["table"] = vm.build_table()

    result["submit_buttons"] = vm.get_filter_buttons()

    return result


@blueprint.route("/dm/ap-invoices", methods=["GET", "POST"])
@response(template_file="shared/_filtertable.html")
@ldap.group_required(["_ERP-Users"])
def ap_invoices():

    vm = APInvoiceViewModel()

    action, result = vm.process_request("APInvoices")

    if action in ["Download", "Redirect"]:
        return result

    result["filters"] = vm.create_filters()

    result["table"] = vm.build_table()

    result["submit_buttons"] = vm.get_filter_buttons()

    return result


@blueprint.route("/dm/purchase-orders", methods=["GET", "POST"])
@response(template_file="shared/_filtertable.html")
@ldap.group_required(["_ERP-Users"])
def purchase_orders():

    vm = POViewModel()

    action, result = vm.process_request("PurchaseOrder")

    if action in ["Download", "Redirect"]:
        return result

    result["filters"] = vm.create_filters()

    result["table"] = vm.build_table()

    result["submit_buttons"] = vm.get_filter_buttons()

    return result


@blueprint.route("/dm/dashboard", methods=["GET", "POST"])
@response(template_file="dm_dashboard.html")
@ldap.group_required(["_ERP-Users"])
def dm_dashboard():

    vm = ViewModelBase()
    return vm.to_dict()


@blueprint.route("/dm/imaging/<doc_type>/<object_id>", methods=["GET", "POST"])
@ldap.group_required(["APP_ImagingUser"])
def imaging_file(doc_type, object_id):

    vm = ImagingViewModel(doc_type, object_id)

    print(vm.path)
    return vm.download_file()

@blueprint.route("/", methods=["GET", "POST"])
@response(template_file="index.html")
def index():

    vm = ViewModelBase()

    return_dict = vm.to_dict()

    return_dict["featured_items"] = get_featured_items("Home Page Items")

    return_dict['text_items'] = get_text_elements('index')

    return return_dict

@blueprint.route("/employee-directory", methods=["GET", "POST"])
@response(template_file="directory.html")
@ldap.login_required
def employee_directory():
    vm = EmployeeDirectoryViewmodel()
    
    vm.get_divisions()

    vm.get_employees()

    vm.get_featured_employees()
    
    return vm.to_dict()

@blueprint.route("/api/employee-images/<employeeid>", methods=["GET", "POST"])
@ldap.login_required
def employee_image(employeeid):
    vm = EmployeeImagesViewmodel(str(employeeid))
    
    print(vm.request_dict.get('fit', 'N'))

    if vm.request_dict.get('fit', 'N').upper() == 'Y':
        return Markup(f'<img class="w-100 h-100" src="/api/employee-images/{employeeid}"></img>')
    else:
        return vm.file


@blueprint.route("/api/division-images/<division>", methods=["GET", "POST"])
@ldap.login_required
def division_image(division):
    vm = DivisionImagesViewmodel(str(division))
    return vm.file


@blueprint.route("/dm/export-document/<docid>", methods=["GET", "POST"])
@ldap.login_required
def export_document(docid):

    vm = ViewModelBase()

    print(vm.login)

    if ',' in docid:
        files = docid.split(',')
    else:
        files = [docid]

    file_list = []

    for file in files:
        try:
            file_list.append(int(file))
        except:
            print(f'{file} cannot be converted to an int')

    print(file_list)
    print(len(file_list))

    if len(file_list) == 1:
        file_data = download_files(vm.login, file_list)
        print(file_data[0][1])
        if file_data:
            return send_file(file_data[0][0], attachment_filename=file_data[0][1], as_attachment=True)
    elif len(file_list) > 1:
        file_data = zip_files(vm.login, file_list)
        if file_data:
            return send_file(file_data, attachment_filename='Documents.zip', as_attachment=True)

    flash("No documents returned", "table-info")
        

    return redirect('/')

@blueprint.route("/forms/asphalt-projections", methods=["GET"])
@response(template_file="asphalt_projections.html")
@ldap.group_required(["APP_Admin", "Managers - All Companies"])
def asphalt_projections():
    vm = AsphaltProjectionsViewmodel()
    vm.get_month()
    vm.get_divisions()
    vm.get_projections()

    return vm.to_dict()

@blueprint.route("/forms/asphalt-projections", methods=["POST"])
@ldap.group_required(["APP_Admin", "Managers - All Companies"])
def asphalt_projections_post():
    vm = AsphaltProjectionsViewmodel()

    vm.process_results()

    return redirect(vm.request_url.replace("&edit=y", ""))



@blueprint.route("/tableau-locations", methods=["GET"])
@response(template_file="tableau_locations.html")
@ldap.login_required
def tableau_locations():
    vm = ViewModelBase()

    vm.page = 'Vehicle Locations'

    return vm.to_dict()

@blueprint.route("/tableau-autorefresh", methods=["GET"])
@response(template_file="tableau/auto_refresh.html")

def tableau_autorefresh():
    return {}


@blueprint.get("/forms/asphalt-projections-2/<division>/<month>")
@blueprint.get("/forms/asphalt-projections-2/<division>")
@blueprint.get("/forms/asphalt-projections-2/")
@blueprint.get("/forms/asphalt-projections-2")
@response(template_file="asphalt_projections/asphalt_projections_v2.html")
@ldap.group_required(["APP_Admin", "Managers - All Companies"])
def asphalt_projections_get(division=None, month=None):
    vm = AsphaltProjectionsV2(division, month)
    vm.table_settings()
    return vm.to_dict()

@blueprint.route("/forms/asphalt-projections-data/<division>/<month>/<type>", methods=["GET"])
@ldap.group_required(["APP_Admin", "Managers - All Companies"])
def asphalt_projections_data_get(division, month, type):
    vm = AsphaltProjectionsV2(division, month)
    
    data = vm.get_data(type)

    return json.dumps(data)

@blueprint.route("/forms/asphalt-projections-data/<division>/<month>/<type>", methods=["POST"])
@ldap.group_required(["APP_Admin", "Managers - All Companies"])
def asphalt_projections_data_post(division, month, type):
    vm = AsphaltProjectionsV2(division, month)

    vm.process_data(type)
    
    return dict()


@blueprint.route("/verkada", methods=["GET"])
@response(template_file="verkada.html")
@ldap.group_required(["APP_Admin", "IT Staff"])
def verkada_get():
    vm = ViewModelBase()
    return vm.to_dict()

@blueprint.route("/forms/asphalt-allocation-defaults", methods=["GET"])
@response(template_file="asphalt_projections/allocation_defaults.html")
@ldap.group_required(["APP_Admin", "Managers - All Companies"])
def asphalt_allocation_defaults_get():
    vm = AsphaltProjectionsV2('000')
    vm.get_allocation_defaults()
    return vm.to_dict()

@blueprint.route("/forms/asphalt-allocation-defaults", methods=["POST"])
@ldap.group_required(["APP_Admin", "Managers - All Companies"])
def asphalt_allocation_defaults_post():
    vm = AsphaltProjectionsV2('000')
    vm.save_allocations()
    return dict()

@blueprint.get("/docs/new")
@blueprint.get("/docs/edit/<category>/<page>")
@response(template_file="docs/docs_edit.html")
@ldap.group_required(["APP_Admin"])
def docs_edit_get(category=None, page=None):
    vm = EditDocViewModel(category, page)
    vm.get_data()
    return vm.to_dict()

@blueprint.post("/docs/edit")
@ldap.group_required(["APP_Admin"])
def docs_edit_post():
    vm = EditDocViewModel(None, None)
    results = vm.process_data()
    return results

@blueprint.get("/docs/<category>/<page>")
@response(template_file="docs/docs.html")
@ldap.login_required
def docs_get(category="", page=""):
    vm = GetDocViewModel(category, page)
    return vm.to_dict()