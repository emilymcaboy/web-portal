from flask import Blueprint, g, request, session, redirect, url_for, flash
from services.active_directory import ldap
from infrastructure.view_modifiers import response
from services.logging import log
from services.ad_functions import get_upn
from viewmodels.shared.viewmodelbase import ViewModelBase

blueprint = Blueprint("account", __name__, template_folder="templates")



@blueprint.route("/login", methods=["GET", "POST"])
@response(template_file="login.html")
def login():
    vm = ViewModelBase()

    next_url = request.args.get('next', '/')
    if g.user:
        return redirect(next_url)
    if request.method == 'POST':
        user = request.form['email']
        pw = request.form['password']

        user = get_upn(user)

        test = ldap.bind_user(user, pw)
        if test is None or pw == '':
            flash("Invalid Login Credentials", "table-danger")

            log(user_name = user,
                status = "Failure",
                page = "Login",
                full_url= str(request.full_path))

            return {'user_id': session.get('user_id')}
        else:
            flash("Successfully logged in", "table-success")
            session['user_id'] = user
            session['lakeside_login'] = 'LAKESIDEIND\\' + ldap.get_object_details(user=session['user_id'])['sAMAccountName'][0].decode('utf-8')
            
            print(f'{session["user_id"]} logged in')

            session.permanent = True

            log(user_name = user,
                status = "Success",
                page = "Login",
                full_url= str(request.full_path))

            
            if next_url:
                return redirect(next_url)
            else:
                return redirect('/')

    return vm.to_dict()

@blueprint.route("/logout", methods=["GET", "POST"])
def logout():

    if 'user_id' not in session.keys():
        return redirect('/')

    print(f'{session["user_id"]} logged out')

    log(user_name = session["user_id"],
                status = "Success",
                page = "Logout",
                full_url= str(request.full_path))

    session.pop('user_id', None)

    flash("Successfully logged out.", 'table-success')
            
    return redirect('/')
