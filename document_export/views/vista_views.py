from viewmodels.shared.viewmodelbase import ViewModelBase
from views.home_views import blueprint
from flask import Blueprint, request, redirect, flash

from infrastructure.view_modifiers import response
from services.active_directory import ldap

from viewmodels.vista.payroll_update_viewmodel import VistaPayrollUpdate
from viewmodels.vista.elevate_permission_viewmodel import ElevateVistaPermissions


blueprint = Blueprint("vista", __name__, template_folder="templates")


@blueprint.route("/vista/payroll-rate-update", methods=["GET"])
@response(template_file="shared/_buttoncards.html")
@ldap.login_required
def payroll_update_get():
    vm = VistaPayrollUpdate()
    vm.build_cards()

    print(vm.filters)

    return vm.to_dict()

@blueprint.route("/vista/payroll-rate-update", methods=["GET", "POST"])
@response(template_file="shared/_buttoncards.html")
@ldap.login_required
def payroll_update_post():
    vm = VistaPayrollUpdate()
    vm.process_results()

    return redirect('/vista/payroll-rate-updated')

@blueprint.route("/vista/elevate-permissions", methods=["GET"])
@response(template_file="vista/elevate_permissions.html")
@ldap.group_required(["IT Staff"])
def elevate_permission_get():
    vm = ElevateVistaPermissions()

    vm.page_header = 'Elevate Permissions'

    return vm.to_dict()

@blueprint.route("/vista/elevate-permissions", methods=["POST"])
@ldap.group_required(["IT Staff"])
def elevate_permission_post():
    vm = ElevateVistaPermissions()
    vm.process_results()

    return redirect('/vista/elevate-permissions')
