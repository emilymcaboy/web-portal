from waitress import serve

import app

serve(app.main, host="0.0.0.0", port=8080)