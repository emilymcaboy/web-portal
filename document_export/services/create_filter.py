from flask import Markup


def create_filter(
    name: str,
    header: str = None,
    filter_type: str = "text",
    value="",
    placeholder=None,
    items=[],
    default_value="",
    tooltip=None,
    step=None,
) -> dict:
    return {
        "name": name,
        "header": header if header else name,
        "type": filter_type,
        "value": "" if not value else value,
        "placeholder": placeholder,
        "items": items,
        "default_value": default_value,
        "tooltip": tooltip,
        "step": step,
    }


class DataTable:
    def __init__(self, ids=[]) -> None:
        self.headers = []
        self.types = []
        self.classes = []
        self.text = []
        self.data = []
        self.default = []
        self.id = ids

    def create_table_col(
        self, col_header, col_type, col_class, col_text, col_data, col_default=""
    ):

        self.headers.append(col_header)
        self.types.append(col_type)
        self.classes.append(col_class)
        self.text.append(col_text)

        if type(col_default) == list:
            self.default.append(col_default)
        else:
            self.default.append([col_default for i in range(len(col_data))])

        rows = len(self.data)

        for i, value in enumerate(col_data):
            if rows == 0:
                self.data.append([])
            self.data[i].append(value)

    def get_table(self):
        return {
            "header": self.headers,
            "type": self.types,
            "class": self.classes,
            "text": self.text,
            "default": self.default,
            "id": self.id,
            "data": self.data,
        }


def get_col(data, col_idx):
    return [row[col_idx] for row in data]


class SubmitButtons:
    def __init__(self):
        self.names = []
        self.classes = []
        self.texts = []
        self.buttons = 0

    def add_button(self, btn_name, btn_class, btn_text):
        self.names.append(btn_name)
        self.classes.append(btn_class)
        self.texts.append(Markup(btn_text))
        self.buttons += 1

    def get_buttons(self):
        return {
            "name": self.names,
            "class": self.classes,
            "text": self.texts,
            "buttons": self.buttons,
        }


class ModalForm:
    def __init__(self) -> None:
        super().__init__()

        self.fields = []


    def add_text(self, header, placeholder='', optional = False):

        self.fields.append(
            {
                "class": 'text',
                "header": header,
                "placeholder": placeholder,
                'optional': optional,
            }
        )

    def add_combo(self, header, keys, values, default, optional = False):
        self.fields.append(
            {
                "class": "combo",
                "header": header,
                "default": default,
                "keys": keys,
                "values": values,
                "optional": optional,
            }
        )

