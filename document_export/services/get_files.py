from services.connections import viewpoint, vp_attachments
from services.get_combo_values import get_plants
from io import BytesIO
from flask import send_file
import zipfile
import time
from PyPDF2 import PdfFileMerger, PdfFileReader
import sys


def get_file_names(
    username,
    plant,
    customer_search,
    customer_job,
    job,
    material,
    ticket,
    start_date,
    end_date,
    limit=20,
    sort=6,
    sort_order=0
):

    if start_date and not end_date:
        end_date = start_date
    elif end_date and not start_date:
        start_date = end_date

    connection = viewpoint()
    cursor = connection.cursor()

    values = [
        username,
        plant,
        customer_search,
        customer_job,
        job,
        material,
        ticket,
        start_date,
        end_date,
        limit,
        sort,
        sort_order
    ]

    cursor.execute("{CALL uspGetMaterialTickets(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}", values)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    return data


def download_files(user, file_list, file_type=None):

    sys.setrecursionlimit(3000)

    connection = viewpoint()
    cursor = connection.cursor()

    file_id = ','.join([str(file) for file in file_list])

    values = [user, file_id, file_type]

    cursor.execute('{CALL uspDownloadFileSecure(?, ?, ?)}', values)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    if len(data) == 0:
        return None
    
    return  [(BytesIO(row[1]), row[0]) for row in data]


def zip_files(user, file_list, file_type=None):

    files = download_files(user, file_list, file_type)

    zf = BytesIO()

    file_counts = {}

    with zipfile.ZipFile(zf, "w", zipfile.ZIP_DEFLATED) as zipf:
        for file in files:
            read_name = str(file[1])
            file_ext = read_name.split(".")[-1]
            file_name = read_name[0 : len(read_name) - len(file_ext) - 1]

            if read_name in file_counts.keys():
                file_counts[read_name] += 1
                final_name = f"{file_name}({file_counts[read_name]}).{file_ext}"
            else:
                file_counts[read_name] = 1
                final_name = read_name

            data = zipfile.ZipInfo(final_name)
            data.date_time = time.localtime(time.time())[:6]
            zipf.writestr(data, file[0].read())

    zf.seek(0)

    return zf


def merge_pdfs(user, file_list):

    files = download_files(user, file_list, ".pdf")

    merged_pdf = PdfFileMerger()

    for file in files:
        if '.cilf.' not in file[1]:
            merged_pdf.append(PdfFileReader(file[0]), file[1])

    output = BytesIO()

    merged_pdf.write(output)

    output.seek(0)

    return output

