from typing import OrderedDict
from flask import Markup, flash

from services.connections import dw_metadata


class DataWarehouse:
    def __init__(self) -> None:
        self.databases = {}
        self.schemas = {}
        self.tables = {}
        self.paths = {}
        self.users = {}
        self.company_settings = {}
        self.divisions = {}
        self.templates = {}
        self.vendor_groups = {}
        self.companies = {}
        self.template_schemas = {}
        self.job_security_levels = {}

    def get_schemas(self, db_id=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        self.schemas = {}

        command = "SELECT SchemaID, SchemaName, SchemaDescription, DatabaseID FROM Schemas WHERE DatabaseID = ? or ? is null AND SchemaID IN(Select DestSchemaID From vTables) ORDER BY SchemaName"

        values = [db_id, db_id]

        cursor.execute(command, values)

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        for row in data:
            self.schemas[row[0]] = {
                "schema_name": row[1],
                "schema_description": row[2],
                "database_id": row[3],
            }

    def get_schema_list(self, id=None, default=None):

        try:
            id = int(id)
        except:
            id = None

        schema_list = []

        if default:
            schema_list.append([default, default, "All"])

        self.get_schemas(id)

        for key, value in self.schemas.items():
            schema_list.append([key, value["schema_name"], value["database_id"]])

        return schema_list

    def get_databases(self):
        connection = dw_metadata()
        cursor = connection.cursor()

        self.databases = {}

        command = "SELECT DatabaseID, DatabaseName, DatabaseDescription FROM Databases WHERE DatabaseID IN(Select DestDatabaseID From vTables)"

        cursor.execute(command)

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        for row in data:
            self.databases[row[0]] = {
                "database_name": row[1],
                "database_description": row[2],
            }

    def get_database_list(self, default=None):
        db_list = []

        if default:
            db_list.append([default, default])

        if len(self.databases) == 0:
            self.get_databases()
        for key, value in self.databases.items():
            db_list.append([key, value["database_name"]])

        return db_list

    def get_tables(self, db_id, schema_id, search=""):
        connection = dw_metadata()
        cursor = connection.cursor()

        self.tables = {}

        values = [db_id, db_id, schema_id, schema_id, search]

        command = """SELECT A.TableID, 
                            A.TableName, 
                            A.DestDatabase,
                            A.DestSchema,
                            ISNULL(B.Source, 'All') as Source,
                            A.Sequence,
                            A.TableDescription
                     FROM vTables as A
                     LEFT JOIN TableSources as B
                        ON A.TableID = B.TableID
                    WHERE (A.DestDatabaseID = ? or ? is null) AND
                          (A.DestSchemaID = ? OR ? is null) AND
                          (A.FullDestinationTableName like '%' + ? + '%')"""

        cursor.execute(command, values)

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        for row in data:
            if row[0] in self.tables.keys():
                self.tables[row[0]]["sources"].append(row[4])
            else:
                self.tables[row[0]] = {
                    "table_name": row[1],
                    "database": row[2],
                    "schema": row[3],
                    "sources": [row[4]],
                    "sequence": row[5],
                    "description": row[6],
                }

    def get_table_list(self, db_id, schema_id, search="", default_source=None):
        try:
            db_id = int(db_id)
        except:
            db_id = None
        try:
            schema_id = int(schema_id)
        except:
            schema_id = None

        self.get_tables(db_id, schema_id, search)

        table_list = []

        for key, value in self.tables.items():

            source_list = []

            if default_source not in value["sources"]:
                source_list.append(default_source)

            source_list.extend(value["sources"])

            table_list.append(
                [
                    key,
                    value["table_name"],
                    value["database"],
                    value["schema"],
                    source_list,
                    value["sequence"],
                    value["description"],
                ]
            )

        return table_list

    def reload_table(self, table_id, source=None, job=False):

        if source == "All":
            source = None

        connection = dw_metadata()
        cursor = connection.cursor()

        values = [int(table_id), source]

        if job:
            cursor.execute("{CALL pReloadTable_Job(?, ?)}", values)
        else:
            values.extend([False, True])
            cursor.execute("{CALL pReloadTable(?, ?, ?, ?)}", values)

        connection.commit()
        connection.close()

    def recreate_hash(self, table_id):

        connection = dw_metadata()
        cursor = connection.cursor()

        values = [int(table_id)]

        cursor.execute("{CALL pRecreateHash_Job(?)}", values)

        connection.commit()
        connection.close()

    def reload_table_task(self, tables: dict):

        connection = dw_metadata()
        cursor = connection.cursor()

        cursor.execute(
            "SET NOCOUNT ON INSERT INTO ReloadTablesTask (StartTime) VALUES(NULL) SELECT SCOPE_IDENTITY() as ID"
        )

        id = cursor.fetchone()[0]

        for i in range(len(tables["id"])):
            values = [id, tables["id"][i], tables["source"][i], tables["sequence"][i]]
            cursor.execute(
                "INSERT INTO ReloadTablesTaskDetails (TaskID, TableID, Source, Sequence) VALUES(?, ?, ?, ?)",
                values,
            )

        cursor.execute("{CALL pRunReloadTableTask_Job(?)}", [id])

        connection.commit()
        connection.close()

        message = f'Reload Task Started.  <a href="https://tableau.lakesideindustries.com/#/views/ReloadTableTaskStatus/ReloadTableTaskStatus?TaskID={id}" target="_blank">View Status</a>'

        flash(Markup(message), "table-info")

    def get_paths(self):
        connection = dw_metadata()
        cursor = connection.cursor()

        self.paths = {}

        command = "SELECT PathID, PathName, PathDescription FROM Paths"

        cursor.execute(command)

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        for row in data:
            self.paths[row[0]] = {
                "path_name": row[1],
                "path_description": row[2],
            }

    def run_path(self, path_id):
        connection = dw_metadata()
        cursor = connection.cursor()

        values = [path_id, True]

        cursor.execute("{CALL pRunPath(?, ?)}", values)

        id = cursor.fetchone()[0]

        connection.commit()
        connection.close()

        message = f'Reload Path Started.  <a href="https://tableau.lakesideindustries.com/#/views/ReloadTableTaskStatus/ReloadTableTaskStatus?TaskID={id}" target="_blank">View Status</a>'

        flash(Markup(message), "table-info")

    def get_streams(self):
        connection = dw_metadata()
        cursor = connection.cursor()

        self.streams = {}

        command = "SELECT StreamID, StreamName, StreamDescription FROM Streams"

        cursor.execute(command)

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        for row in data:
            self.streams[row[0]] = {
                "stream_name": row[1],
                "stream_description": row[2],
            }

    def run_stream(self, path_id):
        connection = dw_metadata()
        cursor = connection.cursor()

        values = [path_id]

        cursor.execute("{CALL pRunStream_Job(?)}", values)

        id = cursor.fetchone()[0]

        connection.commit()
        connection.close()

        message = f'Reload Stream Started.  <a href="https://tableau.lakesideindustries.com/#/views/ReloadTableTaskStatus/ReloadTableTaskStatus?TaskID={id}" target="_blank">View Status</a>'

        flash(Markup(message), "table-info")

    def deploy_security(self):
        connection = dw_metadata()
        cursor = connection.cursor()

        cursor.execute("{CALL security.pDeploySecurity(1)}")

        connection.commit()
        connection.close()

    def get_users(self, user_login=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    A.UserLogin
                    ,CASE
                        WHEN A.UserLogin not like 'LAKESIDEIND\%'
                        THEN A.UserLogin
                        WHEN A.UserLogin like 'LAKESIDEIND\%$'
                        THEN 'Server Account'
                        ELSE ISNULL(P.DisplayName, 'Inactive User')
                    END as UserName
                    ,A.TemplateID
                    ,A.DivisionID
                    ,CAST(A.CreatedDate as date) as CreatedDate
                    ,ISNULL(P.Title, '')
                    ,CASE
                        WHEN E.IsActive IS NULL
                        THEN ''
                        WHEN E.IsActive = 1 THEN 'Active'
                        WHEN E.IsActive = 0 THEN 'Inactive'
                    END as Status
                From security.Users as A
                LEFT JOIN SourceData.ActiveDirectory.Person as P
                    ON P.MSDSPrincipalName = A.UserLogin
                LEFT JOIN DW.PR.dimEmployee as E
                    ON E.UserLogin = A.UserLogin
                """

        if not user_login is None:
            command += " WHERE A.UserLogin = ?"
            cursor.execute(command, [user_login])
        else:
            cursor.execute(command)

        data = cursor.fetchall()

        self.users = {}

        for row in data:
            self.users[row[0]] = {
                "display_name": row[1],
                "template_id": row[2],
                "division_id": row[3],
                "created_date": row[4],
                "title": row[5],
                "status": row[6],
            }

        connection.commit()
        connection.close()

    def save_user(self, user_id, template_id, division_id, action):
        try:
            template_id = int(template_id)
        except:
            template_id = None
        try:
            division_id = int(division_id)
        except:
            division_id = None

        connection = dw_metadata()
        cursor = connection.cursor()

        values = [user_id, division_id, template_id, action]

        try:
            cursor.execute("{CALL security.pSaveUser(?, ?, ?, ?)}", values)
            data = cursor.fetchone()
            connection.commit()
            flash(data[0], "table-success" if data[1] == "Success" else "table-danger")
        except:
            flash("Error saving user", "table-danger")
        finally:
            connection.close()

    def get_templates(self, template_id=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    TemplateID
                    ,TemplateName
                    ,CompanySettingsID
                    ,VendorGroupID
                FROM security.Templates
                """

        if not template_id is None:
            command += " WHERE A.TemplateID = ? ORDER BY TemplateName"
            cursor.execute(command, [template_id])
        else:
            command += " ORDER BY TemplateName"
            cursor.execute(command)

        data = cursor.fetchall()

        self.templates = {}

        for row in data:
            self.templates[row[0]] = {
                "template_name": row[1],
                "company_settings_id": row[2],
                "vendor_group_id": row[3],
            }

        connection.commit()
        connection.close()

    def get_company_settings(self, setting_id=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    CompanySettingID
                    ,SettingDescription
                From security.CompanySettings
                """

        if not setting_id is None:
            command += " WHERE A.CompanySettingID = ?"
            cursor.execute(command, [setting_id])
        else:
            cursor.execute(command)

        data = cursor.fetchall()

        self.company_settings = {}

        for row in data:
            self.company_settings[row[0]] = {
                "setting_description": row[1],
            }

        connection.commit()
        connection.close()

    def get_vendor_groups(self, group_id=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    GroupID
                    ,GroupDescription
                From security.VendorGroups
                """

        if not group_id is None:
            command += " WHERE A.GroupID = ?"
            cursor.execute(command, [group_id])
        else:
            cursor.execute(command)

        data = cursor.fetchall()

        self.vendor_groups = {}

        for row in data:
            self.vendor_groups[row[0]] = {
                "group_description": row[1],
            }

        connection.commit()
        connection.close()

    def get_job_security_levels(self, level_id=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    LevelID
                    ,LevelDescription
                From security.JobSecurityLevels
                """

        if not level_id is None:
            command += " WHERE A.LevelID = ?"
            cursor.execute(command, [level_id])
        else:
            cursor.execute(command)

        data = cursor.fetchall()

        self.job_security_levels = {}

        for row in data:
            self.job_security_levels[row[0]] = {
                "level_description": row[1],
            }

        connection.commit()
        connection.close()

    def get_template_schemas(self, template_id):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    A.SchemaName
                    ,B.CompanyID
                From security.vDWSchemas as A
                LEFT JOIN security.TemplateSchemaAccess as B
                    ON A.SchemaName = B.SchemaID AND
                    B.TemplateID = ?
                """

        cursor.execute(command, [template_id])

        data = cursor.fetchall()

        self.template_schemas = {}

        for row in data:

            if row[0] in self.template_schemas.keys():
                print(row[0])
                self.template_schemas[row[0]].append(row[1])
            else:
                self.template_schemas[row[0]] = [row[1]]

        connection.commit()
        connection.close()

    def get_template_job_security_level(self, template_id):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    A.CompanyID
                    ,A.SecurityLevel
                From security.Schema_JC as A
                WHERE A.TemplateID = ?
                """

        cursor.execute(command, [template_id])

        data = cursor.fetchall()

        self.template_job_security_levels = {}

        for row in data:

            self.template_job_security_levels[row[0]] = row[1]


        connection.commit()
        connection.close()


    def get_template_material_sales_level(self, template_id):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    A.CompanyID
                    ,A.EZStreetOnly
                From security.Schema_MS as A
                WHERE A.TemplateID = ?
                """

        cursor.execute(command, [template_id])

        data = cursor.fetchall()

        self.template_material_sales_levels = {}

        for row in data:

            self.template_material_sales_levels[row[0]] = row[1]


        connection.commit()
        connection.close()

    def get_template_payroll_settings(self, template_id):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    A.CompanyID
                    ,B.PRGroup
                    ,C.ViewHours
                    ,C.ViewEarnings
                    ,C.ViewDeductions
                    ,C.ViewLiabilities
                    ,C.ViewSSN
                    ,C.ViewVision
                    ,C.ViewContactInfo
                    ,C.ViewEEO
                    ,C.ViewStatusDetails
                    ,C.ViewLeave
                From (SELECT 0 as CompanyID 
                    UNION ALL 
                    SELECT 1 
                    UNION ALL 
                    SELECT 2 
                    UNION ALL 
                    SELECT 3) as A
                CROSS JOIN (SELECT 1 as PRGroup 
                            UNION ALL 
                            SELECT 2 
                            UNION ALL 
                            SELECT 3) as B
                LEFT JOIN security.Schema_PR as C
                    ON C.CompanyID = A.CompanyID AND
                    C.PRGroup = B.PRGroup AND
                    C.TemplateID = ?
                ORDER BY CompanyID, PRGroup
                """

        cursor.execute(command, [template_id])

        data = cursor.fetchall()

        self.template_payroll_settings = OrderedDict()

        for row in data:

            if row[0] not in self.template_payroll_settings.keys():
                self.template_payroll_settings[row[0]] = OrderedDict()

            self.template_payroll_settings[row[0]][row[1]] = {
                'hours': row[2],
                'earnings': row[3],
                'deductions': row[4],
                'liabilities': row[5],
                'ssn': row[6],
                'vision': row[7],
                'contact': row[8],
                'eeo': row[9],
                'status': row[10],
                'leave': row[11],
            }

        connection.commit()
        connection.close()

    def get_divisions(self, division_id=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        command = """
                Select
                    DivisionID
                    ,CompanyID
                    ,Division
                    ,DivisionName
                From security.Divisions as A
                """

        if not division_id is None:
            command += " WHERE A.DivisionID = ?"
            cursor.execute(command, [division_id])
        else:
            cursor.execute(command)

        data = cursor.fetchall()

        self.divisions = {}

        for row in data:
            self.divisions[row[0]] = {
                "company_id": row[1],
                "division": row[2],
                "division_name": row[3],
            }

        connection.commit()
        connection.close()

    def save_template_changes(
            self,
            template,
            company_settings,
            vendor_group,
            schemas,
            job_security,
            ez_street,
            payroll,
        ):
        
        connection = dw_metadata()
        cursor = connection.cursor()

        try:
            command = """UPDATE security.Templates
                     SET CompanySettingsID = ?, VendorGroupID = ?
                     WHERE TemplateID = ?"""

            cursor.execute(command, [company_settings, vendor_group, template])

            company_only_schemas = ['AP', 'AR', 'EQ', 'GL']
            schema_has_schema_table = ['AP', 'AR', 'EQ', 'GL', 'JC', 'MS', 'PR']

            command = 'DELETE FROM security.TemplateSchemaAccess Where TemplateID = ?'
            cursor.execute(command, [template])

            for schema in schemas.keys():
                if schema in schema_has_schema_table:
                    command = f"DELETE FROM security.Schema_{schema} WHERE TemplateID = ?"
                    cursor.execute(command, [template])

                for co in schemas[schema]:
                    command = """INSERT INTO security.TemplateSchemaAccess
                                 VALUES (?, ?, ?)"""
                    cursor.execute(command, [template, schema, co])

                    if schema in company_only_schemas:
                        command = f'INSERT INTO security.Schema_{schema} VALUES(?, ?)'
                        cursor.execute(command, [template, co])

                if schema == 'JC':
                    for key, value in job_security.items():
                        if key in schemas['JC']:
                            command = """INSERT INTO security.Schema_JC VALUES(?, ?, ?)"""
                            cursor.execute(command, [template, key, value])

                if schema == 'MS':
                    for key, value in ez_street.items():
                        if key in schemas['MS']:
                            command = 'INSERT INTO security.Schema_MS VALUES(?, ?, ?)'
                            cursor.execute(command, [template, key, value])

                if schema == 'PR':
                    for co, value in payroll.items():
                        if co in schemas['PR']:
                            for group, col in value.items():
                                command = """INSERT INTO security.Schema_PR
                                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

                                values = [
                                    template,
                                    co,
                                    group,
                                    1 if 'hours' in col else 0,
                                    1 if 'earnings' in col else 0,
                                    1 if 'deductions' in col else 0,
                                    1 if 'liabilities' in col else 0,
                                    1 if 'ssn' in col else 0,
                                    1 if 'vision' in col else 0,
                                    1 if 'contact' in col else 0,
                                    1 if 'eeo' in col else 0,
                                    1 if 'status' in col else 0,
                                    1 if 'leave' in col else 0,
                                ]

                                cursor.execute(command, values)


            connection.commit()
            flash("Template updated successfully", "table-success")

        except Exception as e:
            flash(str(e), 'table-danger')
        
        




    def create_new_template(self, name, source_template=None):
        connection = dw_metadata()
        cursor = connection.cursor()

        template_id = None

        try:
            cursor.execute("{CALL security.pCreateNewTemplate(?, ?)}", [name, source_template])
            data = cursor.fetchone()
            connection.commit()
            flash(data[0], 'table-success' if data[1] == 'Success' else 'table-danger')
            if data[1] == 'Success':
                template_id = data[2]
                print(template_id)
            
        except Exception as e:
            flash(str(e), 'table-danger')

        connection.close()

        return template_id

        


    def delete_template(self, template_id):
        connection = dw_metadata()
        cursor = connection.cursor()

        try:
            cursor.execute('{CALL security.pDeleteTemplate(?)}', [template_id])
            cursor.commit()
            flash('Template Deleted Successfully', 'table-success')
            success = True
        except Exception as e:
            flash(str(e), 'table-danger')
            success = False

        connection.close()

        return success



