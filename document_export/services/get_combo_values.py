from services.connections import viewpoint, vp_attachments


def get_plants():

    connection = viewpoint()
    cursor = connection.cursor()

    sql = """
        Select

            Loc

            ,CONCAT(Loc, ' - ', Description)

        From bINLM Where Loc LIKE '__-___' and INCo IN(1, 3) order by Loc

    """

    cursor.execute(sql)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    plants = [("All", "All")]

    for i in data:
        plants.append((i[0], i[1]))

    return plants

def get_po_ordered_by():

    connection = viewpoint()
    cursor = connection.cursor()

    sql = """
        Select Distinct ISNULL(OrderedBy, 'None') 
        From bPOHD as A
        JOIN bHQAT as B
        ON A.UniqueAttchID = B.UniqueAttchID
        WHERE POCo IN(1, 3)
        Order by ISNULL(OrderedBy, 'None')

    """

    cursor.execute(sql)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    ordered_by = ["All"]

    for i in data:
        ordered_by.append(i[0])

    return ordered_by

