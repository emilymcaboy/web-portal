from services.connections import automated_tasks
from flask import flash

def flash_messages(category='All'):
    connection = automated_tasks()

    cursor = connection.cursor()

    values = [
        category
    ]

    cursor.execute('{CALL WebApp.pGetMessages(?)}', values)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    for row in data:
        flash(row[0], row[1])
    return
