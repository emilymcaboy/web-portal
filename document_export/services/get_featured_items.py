from collections import OrderedDict 
from services.connections import automated_tasks
from flask import Markup


def get_featured_items(featured_item_group):

    connection = automated_tasks()
    cursor = connection.cursor()

    command = """SELECT 
                    A.HeaderText, 
                    A.ContentText, 
                    A.ButtonText, 
                    A.ButtonURL
                 FROM WebApp.FeaturedItem as A
                 JOIN WebApp.FeaturedItemGroup as B
                   ON A.FeaturedItemGroupID = B.FeaturedItemGroupID
                 WHERE B.FeaturedItemGroupDescription = ?
                 ORDER BY A.ItemOrder"""

    values = [featured_item_group]

    cursor.execute(command, values)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    data = [[Markup(i) for i in row] for row in data]

    return data

  
def get_headers(access_levels):

  connection = automated_tasks()
  cursor = connection.cursor()

  command = """Select
                A.CategoryID
                ,A.CategoryDescription
                ,B.AccessGroup
                From WebApp.NavigationCategory as A
                JOIN WebApp.AccessGroup as B
                ON A.AccessGroupID = B.AccessGroupID
                Order by Sequence"""

  cursor.execute(command)
  data = cursor.fetchall()

  connection.commit()
  connection.close()

  headers = OrderedDict()

  for row in data:

    if row[2] in access_levels:
      headers[row[0]] = Markup(row[1])

  return headers


def get_header_items(headers, access_levels):
  connection = automated_tasks()
  cursor = connection.cursor()

  command = """SELECT
                A.CategoryID
                ,B.DetailID
                ,B.Type
                ,B.Text
                ,B.URL
                ,ISNULL(S2.AccessGroup, S1.AccessGroup) as DetailAccess
              From WebApp.NavigationCategory as A
              JOIN WebApp.NavigationCategoryDetail as B
                ON A.CategoryID = B.CategoryID
              JOIN WebApp.AccessGroup as S1
                ON S1.AccessGroupID = A.AccessGroupID
              JOIN WebApp.AccessGroup as S2
                ON S2.AccessGroupID = B.AccessGroupID
              Order by B.Sequence"""

  cursor.execute(command)
  data = cursor.fetchall()

  connection.commit()
  connection.close()

  details = OrderedDict()

  for header in headers:
    details[header] = []

  for row in data:

    if row[0] in headers and row[5] in access_levels:
      detail = {
        "type": row[2],
        "text": Markup(row[3]),
        "url": Markup(row[4]),
      }
      details[row[0]].append(detail)

  return details


def get_text_elements(page):

  connection = automated_tasks()
  cursor = connection.cursor()

  command = """Select
                ElementGroup,
                ElementText
              From WebApp.TextElements as A
              WHERE A.Page = ?
              Order by Sequence"""

  values = [page]
  cursor.execute(command, values)
  data = cursor.fetchall()

  connection.commit()
  connection.close()

  headers = {}

  for row in data:

    if row[0] in headers.keys():
      headers[row[0]].append(Markup(row[1]))
    else:
      headers[row[0]] = [Markup(row[1])]

  return headers

  



