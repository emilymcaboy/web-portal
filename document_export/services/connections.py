import pypyodbc


def source_data():
    return pypyodbc.connect(
        "Driver={SQL Server};"
        + "Server=DWSQLSRV;"
        + "Database=SourceData;"
        + "Trusted_Connection=True;"
        + "APP=Lakeside Web App;"
    )

def dw():
    return pypyodbc.connect(
        "Driver={SQL Server};"
        + "Server=DWSQLSRV;"
        + "Database=DW;"
        + "Trusted_Connection=True;"
        + "APP=Lakeside Web App;"
    )

def automated_tasks():
    return pypyodbc.connect(
        "Driver={SQL Server};"
        + "Server=DWSQLSRV;"
        + "Database=AutomatedTasks;"
        + "Trusted_Connection=True;"
        + "APP=Lakeside Web App;"
    )


def dw_metadata():
    return pypyodbc.connect(
        "Driver={SQL Server};"
        + "Server=DWSQLSRV;"
        + "Database=DW_Metadata;"
        + "Trusted_Connection=True;"
        + "APP=Lakeside Web App;"
    )


def viewpoint():
    return pypyodbc.connect(
        "Driver={SQL Server};"
        + "Server=ERPSQLSRV;"
        + "Database=Viewpoint;"
        + "Trusted_Connection=True;"
        + "APP=Lakeside Web App;"
    )


def vp_attachments():
    return pypyodbc.connect(
        "Driver={SQL Server};"
        + "Server=ERPSQLSRV;"
        + "Database=VPAttachments;"
        + "Trusted_Connection=True;"
        + "APP=Lakeside Web App;"
    )
