from flask import render_template, g
from infrastructure.view_modifiers import response
from viewmodels.shared.viewmodelbase import ViewModelBase

@response(template_file='errors/404.html')
def page_not_found(e):

    vm = ViewModelBase()
    return vm.to_dict()

@response(template_file='errors/401.html')
def access_denied(e):

    vm = ViewModelBase()
    return vm.to_dict()