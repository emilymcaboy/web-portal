from services.connections import viewpoint
from flask import Markup, flash



class Vista:
    def __init__(self) -> None:
        self.craft_contracts = {}

    def get_craft_contracts(self, contract_id=None):
        connection = viewpoint()
        cursor = connection.cursor()

        try:
            contract_id = int(contract_id)
        except:
            contract_id = None

        self.craft_contracts = {}

        

        command = "SELECT Co, ContractID, Description FROM budCraftContrMaster WHERE Co = 1 AND (? = ContractID or ? is null) ORDER By ContractID"
        
        values = [contract_id, contract_id]

        cursor.execute(command, values)

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        for row in data:
            ctr_id = str(row[0]) + "|" + str(row[1])
            self.craft_contracts[ctr_id] = {
                'company': row[0],
                'contract_id': row[1],
                'description': row[2],
            }

    def run_craft_contract_master(self, company_id, contract_id, effective_date):
        connection = viewpoint()
        cursor = connection.cursor()

        values = [company_id, contract_id, effective_date]

        try:
            cursor.execute("{CALL usp_Contract_Rate_Update_PRCM(?, ?, ?)}", values)
            data = cursor.fetchone()
            connection.commit()
            flash(data[0], 'table-success')
        except:
            flash("Error updating Effective Dates", 'table-danger')
        finally:
            connection.close()




