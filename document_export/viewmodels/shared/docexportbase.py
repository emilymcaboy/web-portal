from werkzeug.utils import redirect
from viewmodels.shared.viewmodelbase import ViewModelBase
from services.get_files import download_files, zip_files, merge_pdfs
from flask import send_file, redirect, flash
from services.logging import log
from services.generate_query import GenQuery
from services.connections import viewpoint
from services.create_filter import  SubmitButtons


class ExportDocViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()
        self.url = ""
        self.output = ''
        self.files = []
        self.file_id_index = 0
        self.file_total_index = None
        self.page = None
        self.show_filters = False if self.request_dict.get('showfilters', '').upper() == 'N' else True
        self.embed = True if self.request_dict.get('embedded', '').upper() == 'Y' else False
        self.show_header = False if self.request_dict.get('showheader', '').upper() == 'N' else True
        self.attachment_ids = self.request_dict.get("attachmentids", "")

    def process_request(self, file_name = "Download"):
        
        for key in self.request_dict:
            
            if key.startswith('export_'):
                file = download_files(self.login, [int(self.request_dict[key])])
                if file:
                    file, filename = file[0]
                else:
                    flash("There are no documents with that AttachmentID or you do not have access", "table-danger")
                    return ('Redirect', redirect(self.url))
                log(
                    user_name=self.email_login,
                    page=self.page,
                    full_url=self.request_url,
                    unit='Files',
                    total=1,
                    message=f"FileID: {self.request_dict[key]}, File Name: {filename}, Download Single File"

                )
                return ('Download', send_file(file, attachment_filename=filename, as_attachment=True))

            elif key == "ApplyFilters":

                return ('Redirect', redirect(self.url))

        if len(self.files) == 0:
            self.get_files()

        if "download" in self.request_dict.keys() or self.output.upper() == 'ZIP':
            file_ids = []
            for file in self.files:
                file_ids.append(file[self.file_id_index])

            log(
                user_name=self.email_login,
                page=self.page,
                full_url=self.request_url,
                unit='Files',
                total=len(file_ids),
                message="Download ZIP file"

            )
            return ('Download', send_file(
                zip_files(self.login, file_ids), attachment_filename=f'{file_name}.zip', as_attachment=True
            ))
        
        if 'merge' in self.request_dict.keys() or self.output.upper() == 'PDF':
            file_ids = []
            for file in self.files:
                file_ids.append(file[self.file_id_index])

            log(
                user_name=self.email_login,
                page=self.page,
                full_url=self.request_url,
                unit='Files',
                total=len(file_ids),
                message="Download PDF merge file"

            )

            return ('Download', send_file(
                merge_pdfs(self.login, file_ids), attachment_filename=f'{file_name}.pdf', as_attachment=True
            ))

        if len(self.files) > 0 and self.output.upper() == "SCREEN" and self.file_total_index:
            total = self.files[0][self.file_total_index]
            flash(f"Showing {len(self.files)} of {total} attachments", "table-primary")

        return ("Load", self.to_dict())


    def generate_query(self, user, columns, tables, order_by=None, limit = None):
        return GenQuery(viewpoint(), user, columns, tables, order_by, limit)


    def get_filter_buttons(self):

        sb = SubmitButtons()

        sb.add_button(
            btn_name="download",
            btn_class="btn-secondary",
            btn_text='<i class="fa fa-file-archive-o" aria-hidden="true"></i> Download All',
        )

        sb.add_button(
            btn_name="merge",
            btn_class="btn-secondary",
            btn_text='<i class="fa fa-file-pdf-o" aria-hidden="true"></i> Merge and Download PDFs',
        )

        return sb.get_buttons()

    def get_files(self):
        return []

        



