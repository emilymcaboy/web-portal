from datetime import date, timedelta
from flask import redirect
import dateutil

from lakeside.tasks.sharepoint import SharePoint
from lakeside.connection.sql_server import LakesideConnections


from viewmodels.shared.viewmodelbase import ViewModelBase


class MonroePavingViewmodel(ViewModelBase):
    def __init__(self, item_id=None):
        super().__init__()
        self.item_id = item_id

    def process(self):

        lc = LakesideConnections()
        sp = SharePoint()

        self.date = self.request_dict.get('date', date.today())

        if type(self.date) == str:
            self.date = dateutil.parser.parse(self.date)

        self.next = str(self.date + timedelta(days=1))[:10]
        self.previous = str(self.date - timedelta(days=1))[:10]
        self.date = str(self.date)[:10]

        reload = self.request_dict.get('reload', '1')

        if reload == '1' or self.item_id is not None:
            sp.monroe_paving_calendar_to_sql()
        
        if self.item_id is not None:
            return redirect(
                f"https://tableau.lakesideindustries.com/views/MonroeDispatch/MonroeDispatch.pdf?Calendar Item={item_id}&Select By=Item ID&:refresh"
            )

        con = lc.source_data()
        cursor = con.cursor()

        cursor.execute("""
            SELECT Superintendent, Category, Job, StartTime, PDF, Image, SuperintendentView
            FROM SharePoint.vMonroePavingSchedule
            WHERE ScheduleDate = ?""", [self.date])

        data = cursor.fetchall()

        self.superintendents = {}

        for row in data:
            if row[0] not in self.superintendents.keys():
                self.superintendents[row[0]] = []
            self.superintendents[row[0]].append(
                {
                    'category': row[1],
                    'job': row[2],
                    'start': row[3],
                    'pdf': row[4],
                    'image': row[5],
                    'sup_view': row[6]
                }
            
            )

        return self.to_dict()


