from viewmodels.shared.docexportbase import ExportDocViewModel
from services.get_files import get_file_names
from services.get_combo_values import get_plants
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col, SubmitButtons
from flask import Markup




class TicketViewModel(ExportDocViewModel):
    def __init__(self):
        super().__init__()

        self.page = "Material Ticket Export"
        self.page_header = "Material Ticket Image Export"
        self.message_category = 'Tickets'
        self.embed = False

        if "ApplyFilters" in self.request_dict.keys():
            self.plant = self.request_dict.get("PlantFilter", "All")
            self.customer = self.request_dict.get("CustomerSearchFilter", "")
            self.cust_job = self.request_dict.get("CustomerJobPOFilter", "")
            self.job = self.request_dict.get("JobFilter", "")
            self.material = self.request_dict.get("MaterialFilter", "")
            self.ticket = self.request_dict.get("TicketFilter", "")
            self.start_date = self.request_dict.get("StartDate", "")
            self.end_date = self.request_dict.get("EndDate", "")
            self.limit = self.request_dict.get("Limit", 10)
            self.output = "screen"
            self.sort_by = self.request_dict.get("SortBy", 6)
            self.sort_order = self.request_dict.get("SortOrder", 0)
            self.attachment_ids = ''
        else:
            self.plant = self.request_dict.get("plant", "All")
            self.customer = self.request_dict.get("customer", "")
            self.cust_job = self.request_dict.get("custpojob", "")
            self.job = self.request_dict.get("job", "")
            self.material = self.request_dict.get("material", "")
            self.ticket = self.request_dict.get("ticket", "")
            self.start_date = self.request_dict.get("startdate", "")
            self.end_date = self.request_dict.get("enddate", "")
            self.limit = self.request_dict.get("limit", 10)
            self.output = self.request_dict.get("output", "None")
            self.sort_by = self.request_dict.get("sort", 6)
            self.sort_order = self.request_dict.get('sortorder', 0)
            self.attachment_ids = self.request_dict.get('attachmentids', '')

        try:
            self.limit = int(self.limit)
        except:
            self.limit = 10
        try:
            self.sort_by = int(self.sort_by)
        except:
            self.sort_by = 6
        try:
            self.sort_order = int(self.sort_order)
        except:
            self.sort_order = 0

        try:
            self.start_ticket, self.end_ticket = self.ticket.split('-', 1)
        except:
            self.start_ticket, self.end_ticket = (self.ticket, self.ticket)

        if self.end_ticket is None:
            self.end_ticket = self.start_ticket

        self.s_date = self.start_date
        self.e_date = self.end_date

        if self.start_date == '' and self.end_date != '':
            self.s_date = self.end_date
        elif self.start_date != '' and self.end_date == '':
            self.e_date = self.start_date

        self.is_single_file = False
        for key in self.request_dict:
            if key.startswith('export_'):
                self.is_single_file = True
    

        self.file_id_index = 7
        self.file_total_index = 9

        self.url = gen_url(
            "/dm/tickets",
            "output=screen",
            plant=self.plant,
            customer=self.customer,
            custpojob=self.cust_job,
            job=self.job,
            material=self.material,
            ticket=self.ticket,
            startdate=self.start_date,
            enddate=self.end_date,
            sort=self.sort_by if self.sort_by != 6 else None,
            sortorder=self.sort_order if self.sort_order == 1 else None,
            limit=self.limit,
            attachmentids=self.attachment_ids,
        )
    
    def get_files(self):
        if self.output.upper() in ["SCREEN", "ZIP", "PDF"] and not self.is_single_file:
            
            columns = [
                "CONCAT(A.FromLoc, ' - ', C.Description)", # Plant
                "A.Ticket",
                "IIF(SaleType = 'C', CONCAT(CAST(A.Customer as varchar(255)), ' - ', D.Name), CONCAT(dbo.udfTrimJob(A.Job), ' - ', J.Description))", # CustomerOrJob
                "CONCAT(A.Material, ' - ', M.Description)", # Material
                "A.MatlUnits",
                "CAST(B.AddDate as date)",
                "B.OrigFileName",
                "B.AttachmentID",
                "CAST(A.SaleDate as date)",
                "COUNT(*) OVER()", # TotalDocuments
                "IIF(SaleType = 'C', IIF(A.CustJob is null, A.CustPO, IIF(A.CustPO is null, A.CustJob, CONCAT(A.CustJob, ' - ', A.CustPO))), CONCAT(A.MatlPhase, ' - ', P.Description))", # CustJobPOPhase
            ]

            tables = """MSTD as A
                        Join HQAT as B
                            ON A.UniqueAttchID = B.UniqueAttchID
                        JOIN INLM as C
                            ON C.INCo = A.MSCo and C.Loc = A.FromLoc
                        LEFT JOIN ARCM as D
                            ON D.CustGroup = A.CustGroup and D.Customer = A.Customer
                        LEFT JOIN HQMT as M
                            ON M.MatlGroup = A.MatlGroup and M.Material = A.Material
                        LEFT JOIN JCJM as J
                            ON J.JCCo = A.JCCo and A.Job = J.Job
                        LEFT JOIN JCJP as P
                            ON P.JCCo  = A.JCCo and P.Job = A.Job and P.PhaseGroup = A.PhaseGroup and P.Phase = A.MatlPhase"""
            

            order_by = (
                columns[self.sort_by]
                + " "
                + ("ASC " if self.sort_order == 0 else "DESC ")
            )

            self.query = self.generate_query(
                self.login, columns, tables, order_by, self.limit
            )

            if self.plant.upper() != 'ALL':
                self.query.create_where_item(
                    "Plant",
                    "varchar(255)",
                    "A.FromLoc = @Plant",
                    self.plant
                )
            else:
                self.query.create_where_item(
                    "Plant",
                    "varchar(255)",
                    "A.FromLoc like '__-___'",
                    ""
                )
            if self.customer != '':
                self.query.create_where_item(
                    "Customer",
                    "varchar(255)",
                    "UPPER(CONCAT(CAST(A.Customer as varchar(255)), ' - ', D.Name)) LIKE @Customer",
                    "%" + self.customer.upper() + "%"
                )
            if self.cust_job != '':
                self.query.create_where_item(
                    "CustJobPO",
                    "varchar(255)",
                    "UPPER(IIF(SaleType = 'C', IIF(A.CustJob is null, A.CustPO, IIF(A.CustPO is null, A.CustJob, CONCAT(A.CustJob, ' - ', A.CustPO))), CONCAT(A.MatlPhase, ' - ', P.Description))) LIKE @CustJobPO",
                    "%" + self.cust_job.upper() + "%"
                )
            if self.job != '':
                self.query.create_where_item(
                    "Job",
                    "varchar(255)",
                    "UPPER(CONCAT(dbo.udfTrimJob(A.Job), ' - ', J.Description)) LIKE @Job",
                    "%" + self.job.upper() + "%"
                )
            if self.material != '':
                self.query.create_where_item(
                    'Material',
                    "varchar(255)",
                    "UPPER(CONCAT(A.Material, ' - ', M.Description)) Like @Material",
                    "%" + self.material.upper() + "%"
                )
            if self.start_ticket != '':
                self.query.create_where_item(
                    "StartTicket",
                    "varchar(255)",
                    "LTRIM(RTRIM(UPPER(A.Ticket))) >= UPPER(@StartTicket)",
                    self.start_ticket
                )
            if self.end_ticket != '':
                self.query.create_where_item(
                    "EndTicket",
                    "varchar(255)",
                    "LTRIM(RTRIM(UPPER(A.Ticket))) <= UPPER(@EndTicket)",
                    self.end_ticket
                )
            if self.s_date != '':
                self.query.create_where_item(
                    "StartDate",
                    'date',
                    "CAST(A.SaleDate as date) >= @StartDate",
                    self.s_date
                )
            if self.e_date != '':
                self.query.create_where_item(
                    "EndDate",
                    'date',
                    "CAST(A.SaleDate as date) <= @EndDate",
                    self.e_date
                )

            if self.attachment_ids != "":
                self.query.create_multivalue_item(
                    "AttachmentIDs",
                    [self.attachment_ids],
                    "Z",
                    "TRY_CAST(Z.value as int) = B.AttachmentID",
                )

            self.files = self.query.execute_query()
        else:
            self.files = []

    def plants(self):
        return get_plants()

    def create_filters(self):
        filters = [
            create_filter(
                name="PlantFilter",
                header="Plant",
                filter_type="combo",
                value=self.plant,
                items=get_plants(),
                default_value='All'
            ),
            create_filter(
                name="CustomerSearchFilter",
                header="Customer Search",
                filter_type="text",
                placeholder="None",
                value=self.customer,
                default_value='',
                tooltip="Enter customer number or part of name"
            ),
            create_filter(
                name="JobFilter",
                header="LI Job",
                filter_type="text",
                placeholder="None",
                value=self.job,
                default_value='',
                tooltip="Enter job number or part of name"
            ),
            create_filter(
                name="CustomerJobPOFilter",
                header="Cust Job/PO or Phase",
                filter_type="text",
                placeholder="None",
                value=self.cust_job,
                default_value='',
                tooltip="Enter customer job/po or Lakeside phase"
            ),
            create_filter(
                name="MaterialFilter",
                header="Material",
                filter_type="text",
                placeholder="None",
                value=self.material,
                default_value='',
                tooltip="Enter material code or part of name"
            ),
            create_filter(
                name="TicketFilter",
                header="Ticket",
                filter_type="text",
                placeholder="None",
                value=self.ticket,
                default_value='',
                tooltip="Enter ticket number or range (e.g. 10000-10005)"
            ),
            create_filter(
                name="StartDate",
                header="Start Date",
                filter_type="date",
                placeholder="2000-01-01",
                value=self.start_date,
                default_value=''
            ),
            create_filter(
                name="EndDate",
                header="End Date",
                filter_type="date",
                placeholder="2000-01-01",
                value=self.end_date,
                default_value=''
            ),
            create_filter(
                name="Limit",
                header="Limit",
                filter_type="number",
                placeholder="10",
                value=self.limit,
                default_value='10',
                tooltip="Limit the number of tickets returned (up to 1000)"
            ),
            create_filter(
                name="SortBy",
                header="Sort By",
                filter_type="combo",
                value=self.sort_by,
                items=[
                    [0, 'Plant'],
                    [1, 'Ticket'],
                    [2, 'Customer or Job'],
                    [10, 'Customer Job/PO or LI Phase'],
                    [3, 'Material'],
                    [4, 'Units'],
                    [5, 'Added Date'],
                    [8, 'Sale Date'],
                ],
                default_value='6',
                tooltip="Column to use when sorting results"
            ),
            create_filter(
                name="SortOrder",
                header="Sort Order",
                filter_type="combo",
                value=self.sort_order,
                items=[
                    [0, 'Ascending'],
                    [1, 'Descending']
                ],
                default_value='0'
            ),


        ]

        return filters

    def build_table(self):

        if len(self.files) == 0:
            return {}

        dt = DataTable()

        dt.create_table_col(
            col_header="Plant",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 0)
        )

        dt.create_table_col(
            col_header="Ticket",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 1)
        )

        dt.create_table_col(
            col_header="Date",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 8)
        )

        dt.create_table_col(
            col_header="Customer/Job",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 2)
        )

        dt.create_table_col(
            col_header="Cust Job/PO or Phase",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 10)
        )

        dt.create_table_col(
            col_header="Material",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 3)
        )

        dt.create_table_col(
            col_header="Units",
            col_type="",
            col_class="text-right",
            col_text="",
            col_data=get_col(self.files, 4)
        )

        dt.create_table_col(
            col_header="",
            col_type="Download",
            col_class="",
            col_text=Markup('<i class="fa fa-download"></i> Download'),
            col_data=get_col(self.files, 7)
        )

        return dt.get_table()


