from viewmodels.shared.docexportbase import ExportDocViewModel
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col
from services.get_combo_values import get_po_ordered_by
from flask import Markup


class POViewModel(ExportDocViewModel):
    def __init__(self):
        super().__init__()

        self.page = "PO Export"
        self.page_header = "Purchase Order Export"
        self.message_category = "Purchase Orders"

        if "ApplyFilters" in self.request_dict.keys():
            self.vendor = self.request_dict.get("VendorSearchFilter", "")
            self.purchase_order = self.request_dict.get("POSearch", "")
            self.job_loc = self.request_dict.get("JobLocSearch", "")
            self.po_status = self.request_dict.get("POStatus", -1)
            self.start_date = self.request_dict.get("StartDate", "")
            self.end_date = self.request_dict.get("EndDate", "")
            self.limit = self.request_dict.get("Limit", 10)
            self.output = "screen"
            self.sort_by = self.request_dict.get("SortBy", 0)
            self.sort_order = self.request_dict.get("SortOrder", 0)
            self.use_date = self.request_dict.get("UseDate", 0)
            self.ordered_by = self.request_dict.get("OrderedBy", "All")
        else:
            self.vendor = self.request_dict.get("vendor", "")
            self.purchase_order = self.request_dict.get("po", "")
            self.job_loc = self.request_dict.get("jobloc", "")
            self.po_status = self.request_dict.get("postatus", -1)
            self.start_date = self.request_dict.get("startdate", "")
            self.end_date = self.request_dict.get("enddate", "")
            self.limit = self.request_dict.get("limit", 10)
            self.output = self.request_dict.get("output", "None")
            self.sort_by = self.request_dict.get("sort", 0)
            self.sort_order = self.request_dict.get("sortorder", 0)
            self.use_date = self.request_dict.get("usedate", 0)
            self.ordered_by = self.request_dict.get("orderedby", "All")

        try:
            self.limit = int(self.limit)
        except:
            self.limit = 10
        try:
            self.sort_by = int(self.sort_by)
        except:
            self.sort_by = 0
        try:
            self.sort_order = int(self.sort_order)
        except:
            self.sort_order = 0
        try:
            self.use_date = int(self.use_date)
        except:
            self.use_date = 0
        try:
            self.po_status = int(self.po_status)
        except:
            self.po_status = -1

        self.date_fields = ["P.OrderDate", "P.ExpDate", "P.AddedMth", "P.MthClosed"]

        self.is_single_file = False
        for key in self.request_dict:
            if key.startswith("export_"):
                self.is_single_file = True

        self.file_id_index = 12
        self.file_total_index = 5

        self.url = gen_url(
            "/dm/purchase-orders",
            "output=screen",
            vendor=self.vendor,
            po=self.purchase_order,
            jobloc=self.job_loc,
            postatus=self.po_status if self.po_status != -1 else None,
            orderedby=self.ordered_by if self.ordered_by != 'All' else None,
            startdate=self.start_date,
            enddate=self.end_date,
            usedate=self.use_date,
            sort=self.sort_by if self.sort_by != 0 else None,
            sortorder=self.sort_order if self.sort_order == 1 else None,
            limit=self.limit,
        )

        print(self.url)

    def get_files(self):

        if self.output.upper() in ["SCREEN", "ZIP", "PDF"] and not self.is_single_file:

            columns = [
                "P.PO",
                "P.Description",
                "P.OrderedBy",
                "CAST(P.OrderDate as date)",
                "CAST(P.ExpDate as date)",
                "COUNT(*) OVER() as TotalDocuments",
                "CASE WHEN P.Status = 0 THEN 'Open' WHEN P.Status = 2 THEN 'Closed' ELSE 'Unknown' END",
                "CONCAT(V.Vendor, ' - ', V.Name)",
                "CONCAT(ISNULL(dbo.udfTrimJob(J.Job), L.Loc), ' - ', ISNULL(J.Description, L.Description))",
                "IIF(CHARINDEX('.', REVERSE(A.OrigFileName)) - 1 < 0, 'Unknown', RIGHT(A.OrigFileName, CHARINDEX('.', REVERSE(A.OrigFileName)) - 1))",
                "IT.CurUnits",
                "IT.RecvdUnits",
                "A.AttachmentID",
            ]

            tables = """POHD as P
                        JOIN HQAT as A
                            ON P.UniqueAttchID = A.UniqueAttchID
                        JOIN (Select Distinct AttachmentID From HQAI ) as I
                            ON I.AttachmentID = A.AttachmentID
                        LEFT JOIN (
                            SELECT
                                POCo,
                                PO,
                                SUM(CurUnits) as CurUnits,
                                SUM(RecvdUnits) as RecvdUnits
                            FROM POIT
                            Group by POCo, PO) as IT
                            ON IT.POCo = P.POCo AND
                            IT.PO = P.PO
                        JOIN APVM as V
                            ON V.VendorGroup = P.VendorGroup AND
                            V.Vendor = P.Vendor
                        LEFT JOIN JCJM as J
                            ON J.JCCo = P.JCCo AND
                            J.Job = P.Job
                        LEFT JOIN INLM as L
                            ON L.INCo = P.INCo AND
                            L.Loc = P.Loc"""

            order_by = (
                columns[self.sort_by]
                + " "
                + ("ASC " if self.sort_order == 0 else "DESC ")
            )

            self.query = self.generate_query(
                self.login, columns, tables, order_by, self.limit
            )


            if self.vendor != "":
                self.query.create_where_item(
                    "Vendor",
                    "varchar(255)",
                    "UPPER(CONCAT(V.Vendor, ' ', V.Name)) LIKE @Vendor",
                    "%" + self.vendor.upper() + "%",
                )

            if self.purchase_order != "":
                self.query.create_where_item(
                    "PurchaseOrder",
                    "varchar(max)",
                    "UPPER(CONCAT(P.PO, ' ', P.Description)) LIKE @PurchaseOrder",
                    "%" + self.purchase_order.upper() + "%",
                )

            if self.ordered_by != 'All':
                self.query.create_where_item(
                    "OrderedBy",
                    'varchar(max)',
                    'P.OrderedBy = @OrderedBy',
                    self.ordered_by
                )

            if self.job_loc != "":
                self.query.create_where_item(
                    "JobLoc",
                    "varchar(max)",
                    "UPPER(CONCAT(ISNULL(dbo.udfTrimJob(J.Job), L.Loc), ' - ', ISNULL(J.Description, L.Description))) LIKE @JobLoc",
                    "%" + self.job_loc.upper() + "%",
                )

            if self.po_status != -1:
                self.query.create_where_item(
                    "Status", "int", "P.Status = @Status", self.po_status,
                )

            if self.start_date != "":
                self.query.create_where_item(
                    "StartDate",
                    "date",
                    f"CAST({self.date_fields[self.use_date]} as date) >= @StartDate",
                    self.start_date,
                )

            if self.end_date != "":
                self.query.create_where_item(
                    "EndDate",
                    "date",
                    f"CAST({self.date_fields[self.use_date]} as date) <= @EndDate",
                    self.end_date,
                )

            if self.attachment_ids != "":
                self.query.create_multivalue_item(
                    "AttachmentIDs",
                    [self.attachment_ids],
                    "Z",
                    "TRY_CAST(Z.value as int) = A.AttachmentID",
                )

            self.files = self.query.execute_query()
        else:
            self.files = []

    def create_filters(self):
        filters = [
            create_filter(
                name="POSearch",
                header="PO Search",
                filter_type="text",
                placeholder="None",
                value=self.purchase_order,
                default_value="",
                tooltip="Enter the PO or part of the PO description",
            ),
            create_filter(
                name="VendorSearchFilter",
                header="Vendor Search",
                filter_type="text",
                placeholder="None",
                value=self.vendor,
                default_value="",
                tooltip="Enter vendor number or part of name",
            ),
            create_filter(
                name="JobLocSearch",
                header="Job/Location Search",
                filter_type="text",
                placeholder="None",
                value=self.job_loc,
                default_value="",
                tooltip="Enter job/location number or part of the description",
            ),
            create_filter(
                name="POStatus",
                header="Status",
                filter_type="combo",
                value=self.po_status,
                items=[[-1, "All"], [0, "Open"], [2, "Closed"],],
                default_value="-1",
                tooltip="Purchase Order Status",
            ),

            create_filter(
                name="OrderedBy",
                header="Ordered By",
                filter_type="combo",
                value=self.ordered_by,
                items=[[i, i] for i in get_po_ordered_by()],
                default_value="All",
                tooltip="Ordered By",
            ),

            create_filter(
                name="StartDate",
                header="Start Date",
                filter_type="date",
                placeholder="2000-01-01",
                value=self.start_date,
                default_value="",
            ),
            create_filter(
                name="EndDate",
                header="End Date",
                filter_type="date",
                placeholder="2000-01-01",
                value=self.end_date,
                default_value="",
            ),
            create_filter(
                name="UseDate",
                header="Use Date",
                filter_type="combo",
                value=self.use_date,
                items=[
                    [0, "Order Date"],
                    [1, "Expiration Date"],
                    [2, "Added Month"],
                    [3, "Closed Month"],
                ],
                default_value="0",
                tooltip="The date to use when using the start and end date filters",
            ),
            create_filter(
                name="Limit",
                header="Limit",
                filter_type="number",
                placeholder="10",
                value=self.limit,
                default_value="10",
                tooltip="Limit the number of invoices returned (up to 1000)",
            ),
            create_filter(
                name="SortBy",
                header="Sort By",
                filter_type="combo",
                value=self.sort_by,
                items=[
                    [0, "PO"],
                    [1, "PO Description"],
                    [7, "Vendor"],
                    [8, "Job/Location"],
                    [3, "Order Date"],
                    [4, "Expiration Date"],
                    [10, "Current Units"],
                    [11, "Received Units"],
                ],
                default_value="0",
                tooltip="Column to use when sorting results",
            ),
            create_filter(
                name="SortOrder",
                header="Sort Order",
                filter_type="combo",
                value=self.sort_order,
                items=[[0, "Ascending"], [1, "Descending"]],
                default_value="0",
            ),
        ]

        return filters

    def build_table(self):

        if len(self.files) == 0:
            return {}

        dt = DataTable()

        dt.create_table_col(
            col_header="PO",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 0),
        )

        dt.create_table_col(
            col_header="PO Description",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 1),
        )

        dt.create_table_col(
            col_header="Ordered By",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 2),
        )

        dt.create_table_col(
            col_header="Vendor",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 7),
        )

        dt.create_table_col(
            col_header="Job/Location",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 8),
        )

        dt.create_table_col(
            col_header="Order Date",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 3),
        )

        dt.create_table_col(
            col_header="Current Units",
            col_type="",
            col_class="text-right",
            col_text="",
            col_data=[format(value, ",f") for value in get_col(self.files, 10)],
        )

        dt.create_table_col(
            col_header="Received Units",
            col_type="",
            col_class="text-right",
            col_text="",
            col_data=[format(value, ",f") for value in get_col(self.files, 11)],
        )

        dt.create_table_col(
            col_header="Type",
            col_type="",
            col_class="text-center",
            col_text="",
            col_data=[ft.upper() for ft in get_col(self.files, 9)],
        )

        dt.create_table_col(
            col_header="",
            col_type="Download",
            col_class="",
            col_text=Markup('<i class="fa fa-download"></i> Download'),
            col_data=get_col(self.files, 12),
        )

        return dt.get_table()

