from viewmodels.shared.docexportbase import ExportDocViewModel
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col
from flask import Markup


class APInvoiceViewModel(ExportDocViewModel):
    def __init__(self):
        super().__init__()

        self.page = "AP Invoice Export"
        self.page_header = "Accounts Payable Invoice Export"
        self.message_category = "Accounts Payable"
        

        if "ApplyFilters" in self.request_dict.keys():
            self.vendor = self.request_dict.get("VendorSearchFilter", "")
            self.invoice = self.request_dict.get("InvoiceSearch", "")
            self.start_date = self.request_dict.get("StartDate", "")
            self.end_date = self.request_dict.get("EndDate", "")
            self.limit = self.request_dict.get("Limit", 10)
            self.output = "screen"
            self.sort_by = self.request_dict.get("SortBy", 0)
            self.sort_order = self.request_dict.get("SortOrder", 0)
            self.use_date = self.request_dict.get("UseDate", 0)
            self.inv_amount = self.request_dict.get("Amount", "")
        else:
            self.vendor = self.request_dict.get("vendor", "")
            self.invoice = self.request_dict.get("invoice", "")
            self.start_date = self.request_dict.get("startdate", "")
            self.end_date = self.request_dict.get("enddate", "")
            self.limit = self.request_dict.get("limit", 10)
            self.output = self.request_dict.get("output", "None")
            self.sort_by = self.request_dict.get("sort", 0)
            self.sort_order = self.request_dict.get("sortorder", 0)
            self.use_date = self.request_dict.get("usedate", 0)
            self.inv_amount = self.request_dict.get("amount", "")

        try:
            self.limit = int(self.limit)
        except:
            self.limit = 10
        try:
            self.sort_by = int(self.sort_by)
        except:
            self.sort_by = 0
        try:
            self.sort_order = int(self.sort_order)
        except:
            self.sort_order = 0
        try:
            self.use_date = int(self.use_date)
        except:
            self.use_date = 0
        try:
            self.inv_amount = float(self.inv_amount)
        except:
            self.inv_amount = None


        self.date_fields = ['B.InvDate',
                            'B.DueDate',
                            'B.Mth',
                            'C.AddDate']

        self.is_single_file = False
        for key in self.request_dict:
            if key.startswith("export_"):
                self.is_single_file = True

        

        self.file_id_index = 4
        self.file_total_index = 5

        self.url = gen_url(
            "/dm/ap-invoices",
            "output=screen",
            vendor=self.vendor,
            invoice=self.invoice,
            amount=self.inv_amount,
            startdate=self.start_date,
            enddate=self.end_date,
            usedate=self.use_date,
            sort=self.sort_by if self.sort_by != 0 else None,
            sortorder=self.sort_order if self.sort_order == 1 else None,
            limit=self.limit,
        )

    def get_files(self):
        if self.output.upper() in ["SCREEN", "ZIP", "PDF"] and not self.is_single_file:

            columns = [
                "A.Vendor",
                "A.Name",
                "B.APRef",
                "ISNULL(B.Description, '')",
                "C.AttachmentID",
                "COUNT(*) OVER() as TotalDocuments",
                "CAST(B.InvDate as date)",
                "CAST(B.DueDate as date)",
                "B.InvTotal",
                "IIF(CHARINDEX('.', REVERSE(C.OrigFileName)) - 1 < 0, 'Unknown', RIGHT(C.OrigFileName, CHARINDEX('.', REVERSE(C.OrigFileName)) - 1))"
            ]

            tables = """APVM as A
                        JOIN APTH as B
                          ON A.Vendor = B.Vendor and A.VendorGroup = B.VendorGroup
                        JOIN HQAT as C
                          ON C.UniqueAttchID = B.UniqueAttchID"""

            order_by = (
                columns[self.sort_by]
                + " "
                + ("ASC " if self.sort_order == 0 else "DESC ")
            )

            self.query = self.generate_query(
                self.login, columns, tables, order_by, self.limit
            )

            if self.vendor != "":
                self.query.create_where_item(
                    "Vendor",
                    "varchar(255)",
                    "UPPER(CONCAT(A.Vendor, ' ', A.Name)) LIKE @Vendor",
                    "%" + self.vendor.upper() + "%",
                )

            if self.invoice != "":
                self.query.create_where_item(
                    "Invoice",
                    "varchar(max)",
                    "UPPER(CONCAT(B.APRef, ' ', B.Description)) LIKE @Invoice",
                    "%" + self.invoice.upper() + "%",
                )

            if self.inv_amount is not None:
                self.query.create_where_item(
                    "InvoiceAmount",
                    "numeric(18, 2)",
                    "B.InvTotal = @InvoiceAmount",
                    self.inv_amount
                )


            if self.start_date != "":
                self.query.create_where_item(
                    "StartDate",
                    "date",
                    f"CAST({self.date_fields[self.use_date]} as date) >= @StartDate",
                    self.start_date,
                )

            if self.end_date != "":
                self.query.create_where_item(
                    "EndDate",
                    "date",
                    f"CAST({self.date_fields[self.use_date]} as date) <= @EndDate",
                    self.end_date,
                )

            if self.attachment_ids != "":
                self.query.create_multivalue_item(
                    "AttachmentIDs",
                    [self.attachment_ids],
                    "Z",
                    "TRY_CAST(Z.value as int) = C.AttachmentID",
                )

            self.files = self.query.execute_query()
        else:
            self.files = []

    def create_filters(self):
        filters = [
            create_filter(
                name="VendorSearchFilter",
                header="Vendor Search",
                filter_type="text",
                placeholder="None",
                value=self.vendor,
                default_value="",
                tooltip="Enter vendor number or part of name",
            ),
            create_filter(
                name="InvoiceSearch",
                header="Invoice Search",
                filter_type="text",
                placeholder="None",
                value=self.invoice,
                default_value="",
                tooltip="Enter the invoice or part of the invoice description",
            ),
            create_filter(
                name="Amount",
                header="Invoice Amount",
                filter_type="number",
                placeholder="None",
                value=self.inv_amount,
                default_value="",
                tooltip="Enter the exact amount of the invoice.",
                step="0.01",
            ),
            create_filter(
                name="StartDate",
                header="Start Date",
                filter_type="date",
                placeholder="2000-01-01",
                value=self.start_date,
                default_value="",
            ),
            create_filter(
                name="EndDate",
                header="End Date",
                filter_type="date",
                placeholder="2000-01-01",
                value=self.end_date,
                default_value="",
            ),

            create_filter(
                name="UseDate",
                header="Use Date",
                filter_type="combo",
                value=self.use_date,
                items=[
                    [0, "Invoice Date"],
                    [1, "Due Date"],
                    [2, "Invoice Month"],
                    [3, "Attachment Added"],
                ],
                default_value="0",
                tooltip="The date to use when using the start and end date filters",
            ),

            create_filter(
                name="Limit",
                header="Limit",
                filter_type="number",
                placeholder="10",
                value=self.limit,
                default_value="10",
                tooltip="Limit the number of invoices returned (up to 1000)",
            ),
            create_filter(
                name="SortBy",
                header="Sort By",
                filter_type="combo",
                value=self.sort_by,
                items=[
                    [0, "Vendor"],
                    [1, "Vendor Name"],
                    [2, "Invoice"],
                    [3, "Invoice Description"],
                    [6, "Invoice Date"],
                    [7, "Due Date"],
                    [8, "Invoice Amount"],
                ],
                default_value="0",
                tooltip="Column to use when sorting results",
            ),
            create_filter(
                name="SortOrder",
                header="Sort Order",
                filter_type="combo",
                value=self.sort_order,
                items=[[0, "Ascending"], [1, "Descending"]],
                default_value="0",
            ),
        ]

        return filters

    def build_table(self):

        if len(self.files) == 0:
            return {}

        dt = DataTable()

        dt.create_table_col(
            col_header="Vendor",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 0),
        )

        dt.create_table_col(
            col_header="Vendor Name",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 1),
        )

        dt.create_table_col(
            col_header="Invoice",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 2),
        )

        dt.create_table_col(
            col_header="Invoice Description",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 3),
        )

        dt.create_table_col(
            col_header="Invoice Date",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 6),
        )

        dt.create_table_col(
            col_header="Due Date",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.files, 7),
        )

        dt.create_table_col(
            col_header="Amount",
            col_type="",
            col_class="text-right",
            col_text="",
            col_data=[format(value, ',f') for value in get_col(self.files, 8)],
        )

        dt.create_table_col(
            col_header="Type",
            col_type="",
            col_class="text-center",
            col_text="",
            col_data=[ft.upper() for ft in get_col(self.files, 9)],
        )

        dt.create_table_col(
            col_header="",
            col_type="Download",
            col_class="",
            col_text=Markup('<i class="fa fa-download"></i> Download'),
            col_data=get_col(self.files, 4),
        )

        return dt.get_table()

    

