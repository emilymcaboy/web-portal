from viewmodels.shared.viewmodelbase import ViewModelBase
import magic
from mimetypes import guess_extension
from math import ceil
from flask import send_file
from io import BytesIO


class ImagingViewModel(ViewModelBase):
    def __init__(self, doc_type, object_id) -> None:
        super().__init__()
        self.doc_type = doc_type
        self.object_id = int(object_id)
        self.folder = ceil((self.object_id) / 1024) - 1
        self.path = f"\\\\IMAGING001\\imaging\\{self.doc_type}\\0\\{self.folder}\\{self.object_id}.bin"        
        
        self.mime_type = magic.from_file(self.path, mime=True)
        self.extension = guess_extension(self.mime_type)

        self.file_name = f"{self.object_id}{self.extension}"


    def download_file(self):

        with open(self.path, 'rb') as f:
            return send_file(BytesIO(f.read()), attachment_filename=self.file_name, as_attachment=True)
