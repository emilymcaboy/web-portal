from typing import final
from flask import request, Markup
from viewmodels.shared.viewmodelbase import ViewModelBase
from lakeside.connection.sql_server import LakesideConnections
import json
import markdown


class GetDocViewModel(ViewModelBase):
    def __init__(self, category="", page=""):
        super().__init__()
        self.page_title = "No Matching Page"
        self.content = "# No Matching Page\n\nThere is no page matching your request."

        self.category = category
        self.page = page

        self.lc = LakesideConnections(app="Web App Get Docs")

        self.edit_url = f'/docs/edit/{category}/{page}'

        self.get_data()
        self.convert_content_to_html()

    def get_data(self):
        try:
            con = self.lc.automated_tasks()
            cursor = con.cursor()

            command = """
                SELECT
                    PageName,
                    PageContent
                FROM Docs.Page as A
                JOIN Docs.Category as B
                    ON A.CategoryID = B.CategoryID
                
                WHERE B.CategoryURL = ? AND
                      A.PageURL = ?"""

            values = [
                self.category,
                self.page,
            ]

            cursor.execute(command, values)
            data = cursor.fetchone()

            if data:
                self.page_title = data[0]
                self.content = data[1]

        except Exception as e:
            raise e
        finally:
            con.close()

    def convert_content_to_html(self):

        text = markdown.markdown(self.content, extensions=['tables', 'fenced_code'])

        text = text.replace('<table>', '<table class="table table-striped table-sm">')

        self.html = Markup(text)
