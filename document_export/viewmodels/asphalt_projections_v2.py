from flask.globals import request
from numpy import append
from viewmodels.shared.viewmodelbase import ViewModelBase
from lakeside.connection.sql_server import LakesideConnections
import json
from flask import Markup
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import pandas as pd


class AsphaltProjectionsV2(ViewModelBase):
    def __init__(self, division=None, month=None):
        super().__init__(container_type="container-fluid")

        self.lc = LakesideConnections(app="Asphalt Projections")

        self.page = "Asphalt Projections"
        self.page_header = "Asphalt Projections"

        self.division = self.get_default_division() if division is None else division
        self.month = month
        self.disabled_col = "#dddddd"
        self.base_link = "/forms/asphalt-projections-2"

        self.get_month()
        #ORIGINAL
        self.month_status = {
             "dec": self.month_number == 11,
             "jan": self.month_number >= 11,
             "feb": self.month_number >= 11 or self.month_number < 2,
             "mar": self.month_number >= 11 or self.month_number < 3,
             "apr": self.month_number >= 11 or self.month_number < 4,
             "may": self.month_number >= 11 or self.month_number < 5,
             "jun": self.month_number >= 11 or self.month_number < 6,
             "jul": self.month_number >= 11 or self.month_number < 7,
             "aug": self.month_number >= 11 or self.month_number < 8,
             "sep": self.month_number >= 11 or self.month_number < 9,
             "oct": self.month_number >= 11 or self.month_number < 10,
             "nov": True,
             "future": True,
         }

        # self.month_status = {
        #     "dec": self.month_number == 1,
        #     "jan": self.month_number == 2,
        #     "feb": self.month_number == 3,
        #     "mar": self.month_number == 4,
        #     "apr": self.month_number == 5,
        #     "may": self.month_number == 6,
        #     "jun": self.month_number == 7,
        #     "jul": self.month_number == 8,
        #     "aug": self.month_number == 9,
        #     "sep": self.month_number == 10,
        #     "oct": self.month_number == 11,
        #     "nov": self.month_number == 12,
        #     "future": True,
        # }


    def table_settings(self):
        self.get_asphalt_sources()
        self.get_plants()
        self.get_liquid_types()
        self.get_divisions()
        self.month_options()
        self.get_defaults()
        self.get_division_status()
        self.get_month_status()
        self.get_customers()
        self.get_pending_job_status()
        self.get_prior_history_totals()

    def get_data(self, type):
        if type == "current":
            return self.get_current_job_data()
        elif type == "pending":
            return self.get_pending_job_data()
        elif type == "plants":
            return self.get_material_sales_data()
        elif type == "ezstreet":
            return self.get_ez_street_data()

    def get_ez_street_data(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            "{ CALL AsphaltProjections.pGetEZStreetHistory(?, ?)}",
            [self.division, self.month],
        )

        data = cursor.fetchall()

        cursor.close()

        rows = []

        for row in data:
            tot_f = 0

            for i in range(0, 13):
                tot_f += row[i]

            data_dict = {
                "dec": row[0],
                "jan": row[1],
                "feb": row[2],
                "mar": row[3],
                "apr": row[4],
                "may": row[5],
                "jun": row[6],
                "jul": row[7],
                "aug": row[8],
                "sep": row[9],
                "oct": row[10],
                "nov": row[11],
                "tot_f": tot_f,
                "current": row[12],
            }

            rows.append(data_dict)

        return rows


    def get_prior_history_totals(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()
        cursor.execute("{ CALL AsphaltProjections.pGetPriorHistoryTotals(?, ?, ?)}", [self.division, self.month, 'Plant'])
        plant_data = cursor.fetchall()
        cursor.execute("{ CALL AsphaltProjections.pGetPriorHistoryTotals(?, ?, ?)}", [self.division, self.month, 'Job'])
        job_data = cursor.fetchall()

        self.plant_history = Markup(json.dumps(self.list_to_dict(plant_data)))
        self.job_history = Markup(json.dumps(self.list_to_dict(job_data)))




    def list_to_dict(self, data):
        return {i[0]: i[1] for i in data}



    def get_divisions(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute("{ CALL AsphaltProjections.pGetDivisions(?) }", [self.month])

        self.divisions = cursor.fetchall()

        cursor.close()

    def get_pending_job_data(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            "{ CALL AsphaltProjections.pGetPendingHistory(?, ?)}",
            [self.division, self.month],
        )

        data = cursor.fetchall()

        cursor.close()

        jobs = []

        for id, row in enumerate(data):
            tot_f = 0
            check = 0
            for i in range(7, 19):
                tot_f += row[i]

            data_dict = {
                "job": row[0],
                "job_status": row[1],
                "plant": row[2],
                "liquid": row[3],
                "pct": "{:.2f}".format(row[4]),
                "source": row[5],
                "actual_ttg": row[6],
                "dec": row[7],
                "jan": row[8],
                "feb": row[9],
                "mar": row[10],
                "apr": row[11],
                "may": row[12],
                "jun": row[13],
                "jul": row[14],
                "aug": row[15],
                "sep": row[16],
                "oct": row[17],
                "nov": row[18],
                "tot_f": tot_f + row[20],
                "future": row[19],
                "total": tot_f + row[20] + row[19],
                "current": row[20],
                "id": id,
            }

            for key, value in self.month_status.items():
                if value:
                    check += data_dict[key]

            data_dict["check"] = data_dict["actual_ttg"] - check - data_dict["current"]

            jobs.append(data_dict)

        return jobs

    def get_material_sales_data(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            "{ CALL AsphaltProjections.pGetPlantData(?, ?) }",
            [self.division, self.month],
        )

        data = cursor.fetchall()
        cursor.close()
        result = []

        for id, row in enumerate(data):
            tot_f = 0
            for i in range(1, 13):
                tot_f += row[i]
            result.append(
                {
                    "plant": row[0],
                    "liquid": row[14],
                    "pct": "{:.2f}".format(row[13]),
                    "source": row[17],
                    "dec": row[1],
                    "jan": row[2],
                    "feb": row[3],
                    "mar": row[4],
                    "apr": row[5],
                    "may": row[6],
                    "jun": row[7],
                    "jul": row[8],
                    "aug": row[9],
                    "sep": row[10],
                    "oct": row[11],
                    "nov": row[12],
                    "tot_f": tot_f + row[16],
                    "customer": row[15],
                    "current": row[16],
                    "id": id,
                }
            )

        return result

    def get_current_job_data(self):

        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            "{ CALL AsphaltProjections.pGetHistory(?, ?) }", [self.division, self.month]
        )

        data = cursor.fetchall()

        cursor.close()

        jobs = []

        for id, row in enumerate(data):
            tot_f = 0
            check = 0
            for i in range(6, 18):
                tot_f += row[i]

            data_dict = {
                "job": row[1],
                "description": row[2],
                "complete": row[22],
                "plant": row[19],
                "liquid": row[21],
                "pct": "{:.2f}".format(row[20]),
                "source": row[23],
                "tons": row[3],
                "tons_comp": row[4],
                "tons_tg": row[5],
                "actual_ttg": row[24],
                "dec": row[6],
                "jan": row[7],
                "feb": row[8],
                "mar": row[9],
                "apr": row[10],
                "may": row[11],
                "jun": row[12],
                "jul": row[13],
                "aug": row[14],
                "sep": row[15],
                "oct": row[16],
                "nov": row[17],
                "tot_f": tot_f + row[25],
                "future": row[18],
                "total": tot_f + row[18] + row[25],
                "current": row[25],
                "id": id,
            }

            for key, value in self.month_status.items():
                if value:
                    check += data_dict[key]

            data_dict["check"] = data_dict["actual_ttg"] - check - data_dict["current"]

            jobs.append(data_dict)
        return jobs

    def get_month(self):
        connection = self.lc.automated_tasks()
        cursor = connection.cursor()

        if self.month is None:
            command = """Select 
                            DATEADD(month, -1, DATEADD(day, 1, EOMONTH(GETDATE(), -1))), 
                            DATEADD(day, 1, EOMONTH(GETDATE(), -1)), 
                            DATEADD(month, 1, DATEADD(day, 1, EOMONTH(GETDATE(), -1))), 
                            MONTH(GETDATE()),
                            YEAR(DATEADD(month, 2, GETDATE()))"""
            cursor.execute(command)
        else:
            command = """DECLARE @Month date
                     SET @Month = ISNULL(TRY_CAST(? as date), DATEADD(day, 1, EOMONTH(GETDATE(), -1)))

                     Select 
                        DATEADD(month, -1, @Month), 
                        @Month, 
                        DATEADD(month, 1, @Month),
                        MONTH(@Month),
                        YEAR(DATEADD(month, 2, @Month))"""

            cursor.execute(command, [self.month])

        data = cursor.fetchone()

        cursor.close()

        self.month = data[1]

        self.prior_month = data[0]
        self.next_month = data[2]
        self.month_number = data[3]
        self.fiscal_year = data[4]
        self.month_abbr = parse(data[1]).strftime("%b").lower()

    def get_asphalt_sources(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            "SELECT AsphaltSource From AsphaltProjections.AsphaltSources ORDER BY SortOrder"
        )

        data = cursor.fetchall()

        cursor.close()

        self.asphalt_sources = Markup(json.dumps([[i[0] for i in data]]))[1:-1]

    def get_plants(self):
        con = self.lc.dw()
        cursor = con.cursor()

        cursor.execute(
            """Select 
                            LocationWithDescription
                          From IV.dimLocation
                          WHERE LocationType = 'Asphalt Plant' and 
                                IsActive = 1 
                          ORDER BY IIF(Division = ?, 0, 1), LocationWithDescription""",
            [self.division],
        )

        data = cursor.fetchall()

        self.plants = Markup(json.dumps([[i[0] for i in data]]))[1:-1]

    def get_pending_job_status(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            """Select 
                    JobStatus
                From AsphaltProjections.PendingJobStatus
                ORDER BY SortOrder""",
        )

        data = cursor.fetchall()

        self.pending_job_status = Markup(json.dumps([[i[0] for i in data]]))[1:-1]

    def get_customers(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            """Select 
                    CustomerWithName
                From AsphaltProjections.ActiveMSCustomers"""
        )

        data = cursor.fetchall()

        self.customers = Markup(json.dumps([[i[0] for i in data]]))[1:-1]

    def get_defaults(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        command = """Select

                    B.LocationWithDescription

                    ,L.LiquidType

                    ,ISNULL(PD.AsphaltPercent, A.AsphaltPercent) as AsphaltPercent

                From AsphaltProjections.DivisionDefaults as A
                LEFT JOIN DW.IV.dimLocation as B
                    ON A.PlantKey = B.LocationKey
                LEFT JOIN AsphaltProjections.LiquidTypes as L
                    ON L.LiquidTypeID = A.LiquidTypeID
                LEFT JOIN AsphaltProjections.PlantDefaults as PD
                    ON PD.LocationKey = A.PlantKey
                WHERE A.Division = ?"""

        cursor.execute(command, [self.division])
        data = cursor.fetchone()

        self.default_plant, self.default_liquid, self.default_pct = tuple(data)

    def get_liquid_types(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        cursor.execute(
            "SELECT LiquidType From AsphaltProjections.LiquidTypes ORDER BY SortOrder"
        )

        data = cursor.fetchall()

        self.liquid_types = Markup(json.dumps([[i[0] for i in data]]))[1:-1]

    def month_options(self):
        self.month_options = []
        month = parse(self.month) - relativedelta(months=5)

        for _ in range(9):
            month = month + relativedelta(months=1)
            self.month_options.append(
                {
                    "month": month.strftime("%b"),
                    "current": str(month)[:10] == self.month,
                    "link": f"{self.base_link}/{self.division}/{str(month)[:10]}",
                    "date": str(month)[:10],
                }
            )

    def get_default_division(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        command = """SELECT TOP 1

                        Division

                    FROM AsphaltProjections.CurrentJobs
                    WHERE UserName = ?
                    ORDER BY Month DESC"""

        cursor.execute(command, [self.user])
        data = cursor.fetchone()

        if data:
            return data[0]
        else:
            return "020"

    def process_data(self, type):

        data = request.get_json(silent=True)

        if type == "current":
            self.process_current_jobs(data)
        if type == "pending":
            self.process_pending_jobs(data)
        if type == "plants":
            self.process_plants(data)
        if type == "ezstreet":
            self.process_ez_street(data)

    def process_ez_street(self, data):

        df = pd.DataFrame(data)

        df["division"] = self.division
        df["month"] = self.month
        df["user"] = self.user

        self.lc.execute_command(self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pRemovePendingData(?, ?, ?) }",
            ['EZStreet', self.division, self.month])

        self.lc.df_to_sql(
            self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
            "AsphaltProjections",
            df,
            "EZStreet",
            append=True,
        )

        self.lc.execute_command(
            self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pMoveEZStreet(?, ?) }",
            [self.division, self.month],
        )

    def process_plants(self, data):

        df = pd.DataFrame(data)

        df["division"] = self.division
        df["month"] = self.month
        df["user"] = self.user

        self.lc.execute_command(self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pRemovePendingData(?, ?, ?) }",
            ['Plants', self.division, self.month])

        if "id" in df.columns:
            df = df.drop("id", 1)

        self.lc.df_to_sql(
            self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
            "AsphaltProjections",
            df,
            "Plants",
            append=True,
        )

        self.lc.execute_command(
            self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pMovePlants(?, ?) }",
            [self.division, self.month],
        )

    def process_current_jobs(self, data):

        df = pd.DataFrame(data)

        df["division"] = self.division
        df["month"] = self.month
        df["user"] = self.user

        self.lc.execute_command(self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pRemovePendingData(?, ?, ?) }",
            ['Current', self.division, self.month])

        if "id" in df.columns:
            df = df.drop("id", 1)

        self.lc.df_to_sql(
            self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
            "AsphaltProjections",
            df,
            "CurrentJobs",
            append=True,
        )

        self.lc.execute_command(
            self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pMoveCurrentJobs(?, ?) }",
            [self.division, self.month],
        )

    def process_pending_jobs(self, data):
        df = pd.DataFrame(data)

        df["division"] = self.division
        df["month"] = self.month
        df["user"] = self.user

        self.lc.execute_command(self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pRemovePendingData(?, ?, ?) }",
            ['Pending', self.division, self.month])

        if "id" in df.columns:
            df = df.drop("id", 1)

        self.lc.df_to_sql(
            self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
            "AsphaltProjections",
            df,
            "PendingJobs",
            append=True,
        )

        self.lc.execute_command(
            self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pMovePendingJobs(?, ?) }",
            [self.division, self.month],
        )

    def get_month_status(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        command = """
            DECLARE @Division varchar(25) = ?
                    ,@Month date = ?

            Select DISTINCT

                Month

            From AsphaltProjections.CurrentJobs
            WHERE Division = @Division AND
                Month BETWEEN DATEADD(month, -4, @Month) AND DATEADD(month, 4, @Month)
            """
        cursor.execute(command, [self.division, self.month])

        data = cursor.fetchall()

        data = [str(i[0])[:10] for i in data]

        self.month_submit_status = {}

        for mth in self.month_options:
            if mth["date"] in data:
                self.month_submit_status[mth["date"]] = "table-success"
            else:
                self.month_submit_status[mth["date"]] = "table-active"

            if mth["date"] == self.month:
                self.month_submit_status[mth["date"]] += " border border-dark"

    def get_division_status(self):
        con = self.lc.automated_tasks()
        cursor = con.cursor()

        command = """
            DECLARE @Month date = ?

            Select DISTINCT

                Division

            From AsphaltProjections.CurrentJobs
            WHERE Month = @Month
            """
        cursor.execute(command, [self.month])

        data = [i[0] for i in cursor.fetchall()]

        self.division_status = {}

        for div in self.divisions:
            if div[0] in data:
                self.division_status[div[0]] = "table-success"
            else:
                self.division_status[div[0]] = "table-active"

            if div[0] == self.division:
                self.division_status[div[0]] += " border border-dark"

    def get_allocation_defaults(self):
        try:
            con = self.lc.automated_tasks()
            cursor = con.cursor()

            cursor.execute(
                "SELECT AsphaltSource From AsphaltProjections.AsphaltSources ORDER BY SortOrder"
            )

            data = cursor.fetchall()

            data = [i[0] for i in data if i[0] != "Open"]
            data.insert(0, "")

            self.asphalt_sources = Markup(json.dumps([data]))[1:-1]
        except Exception as e:
            raise e
        finally:
            con.close()

        try:
            con = self.lc.automated_tasks()
            cursor = con.cursor()

            command = """
                SELECT
                    PlantKey, 
                    Plant, 
                    PlantDescription, 
                    DefaultAsphaltSource,
                    SecondaryAsphaltSource,
                    PrimaryPercent
                FROM AsphaltProjections.vAllocationDefaults"""

            cursor.execute(command)
            data = cursor.fetchall()
        except Exception as e:
            raise e
        finally:
            con.close()

        self.allocation_defaults = []

        for row in data:
            self.allocation_defaults.append(
                {
                    "plant_key": row[0],
                    "location": row[1],
                    "description": row[2],
                    "default": row[3],
                    "secondary": row[4],
                    "pct": float(row[5]),
                    "status": 0,
                }
            )

        self.allocation_defaults = Markup(json.dumps(self.allocation_defaults))

    def save_allocations(self):
        data = request.get_json(silent=True)

        self.lc.execute_command(
            self.lc.automated_tasks(),
            "{ CALL AsphaltProjections.pAddUpdateAllocationDefault(?, ?, ?, ?) }",
            [
                data["plant_key"],
                data.get("default"),
                data.get("secondary"),
                data.get("pct"),
            ],
        )
