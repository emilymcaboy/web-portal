from operator import itemgetter

from flask.globals import request
from viewmodels.shared.viewmodelbase import ViewModelBase
from services.create_filter import create_filter
from services.vista import Vista
from flask import Markup, jsonify


class VistaPayrollUpdate(ViewModelBase):
    def __init__(self):
        super().__init__()

        self.page = "Payroll Update"
        self.page_header = "Vista Payroll Update"
        self.header_text = "Test"
        self.vista = Vista()

    def build_cards(self):
        self.cards = []

        self.cards.append(
            {
                "header": "Craft Master",
                "id": "Craft Master",
                "text": "Update the effective date for crafts " +
                        "covered by the selected contract. The " +
                        "effective date can be specified above. " +
                        "If no effective date is given, the " +
                        "current date will be used. This cannot " +
                        "be run by template, so setting the Template " +
                        "above will have no effect on this task.",
            }
        )

        self.cards.append(
            {
                "header": "PR Craft Class",
                "id": "Craft Class",
                "text": "Update the New Total Wage Rate on the Info Tab in PR Craft Classes.",
            }
        )

        self.cards.append(
            {
                "header": "PR Craft Class Base Rates",
                "id": "Craft Class Rates",
                "text": "Update the rates on the Pay Rates tab in PR Craft Classes.",
            }
        )

        self.cards.append(
            {
                "header": "PR Craft Class Burden",
                "id": "Craft Class Burden",
                "text": "Update the Burden Rate and the Fixed Burden on the Info tab in PR Craft Classes.",
            }
        )

        self.cards.append(
            {
                "header": "PR Craft Class Variable Rates",
                "id": "Craft Class Variable",
                "text": "Update the Variable Rates on the Variable Earnings tab in PR Craft Classes.",
            }
        )

        self.cards.append(
            {
                "header": "PR Template Base Rates",
                "id": "Template Base Rates",
                "text": "Update the template base rates on the Pay Rates tab in PR Craft Class Templates.",
            }
        )

        self.cards.append(
            {
                "header": "PR Template Variable Rates",
                "id": "Template Base Rates",
                "text": "Update the template variable rates on the Variable Earnings tab in PR Craft Class Templates.",
            }
        )

        self.cards.append(
            {
                "header": "PR Template Variable Rates",
                "id": "Template Base Rates",
                "text": "Update the template variable rates on the Variable Earnings tab in PR Craft Class Templates.",
            }
        )

        self.vista.get_craft_contracts()

        default_contract = [i for i in self.vista.craft_contracts.keys()][0]

        self.filters = [
            create_filter(
                name="CraftContract",
                header="Craft Contract",
                filter_type="combo",
                value=default_contract,
                items=[
                    [
                        i,
                        str(self.vista.craft_contracts[i]["contract_id"])
                        + " - "
                        + self.vista.craft_contracts[i]["description"],
                    ]
                    for i in self.vista.craft_contracts.keys()
                ],
                default_value=default_contract,
            ),
            create_filter(
                name="EffectiveDate",
                header="Effective Date",
                filter_type="date",
                placeholder="2000-01-01",
                value="",
                default_value="",
            ),
            create_filter(
                name="Template",
                header="Template",
                filter_type="number",
                placeholder="",
                value="",
                default_value="",
            ),
        ]

    def process_results(self):

        for key, value in request.form.items():
            print(key, value)
