from flask.globals import request
from flask import flash, Markup

import folium
from folium.plugins import MarkerCluster
from jinja2.loaders import ChoiceLoader
import pickle

from lakeside.tasks.geotab import Geotab

from statistics import mean

from viewmodels.shared.viewmodelbase import ViewModelBase
from services.connections import dw, automated_tasks
from collections import OrderedDict


class TruckLocations(ViewModelBase):
    def __init__(self) -> None:
        

        super().__init__()

        self.page = "Truck Locations"
        try:
            self.client = pickle.load(open("Geotab.p", 'rb'))
            self.gt = Geotab(existing_client=self.client)
        except:
            self.gt = Geotab()
            pickle.dump(self.gt.client, open("Geotab.p", 'wb'))


    def __get_plants(self):
        con = dw()
        cursor = con.cursor()

        command = '''Select 
                        Location,                --0
                        LocationDescription,     --1
                        LocationWithDescription, --2
                        Latitude,                --3
                        Longitude,               --4
                        ActiveToday              --5
                     From IV.ActivePlantsToday'''

        cursor.execute(command)

        data = cursor.fetchall()

        return data


    def map_current_locations(self):
        device_locations = self.gt.get_current_device_status()
        devices = self.gt.get_devices()
        drivers = self.gt.get_users(drivers_only=True)

        latitudes = [status["latitude"] for _, status in device_locations.items()]
        longitudes = [status["longitude"] for _, status in device_locations.items()]

        tooltip="Click to view Details"

        active_plants = self.__get_plants()


        m = folium.Map(location=[mean(latitudes), mean(longitudes)], zoom_start=8)

        for dev_id, status in device_locations.items():

            if type(status['driver']) == dict:
                try:
                    driver_name = f"{drivers[status['driver']['id']].get('firstName', '')} {drivers[status['driver']['id']].get('lastName', '')}"
                except:
                    driver_name = "Unknown Driver"
            else:
                driver_name = "Unknown Driver"

            vehicle_name = devices.get(dev_id)['name']

            popup_text = f"<h4>{vehicle_name}</h4>"
            popup_text += f"Driver: <strong>{driver_name}</strong><br>"
            popup_text += f"Bearing: <strong>{status.get('bearing', '')}</strong><br>"
            popup_text += f"Speed: <strong>{status.get('speed', '0')} kph</strong>"

            iframe = folium.Html(popup_text, script=True)

            if status.get('latitude'):
                folium.Marker(
                    location=[status.get('latitude'), status.get('longitude')],
                    tooltip=tooltip,
                    popup=folium.Popup(iframe, max_width=450),
                    icon=folium.Icon(icon='truck', prefix='fa', color="green" if status["isDriving"] else 'red')
                ).add_to(m)

        for row in active_plants:
            status = "Active" if row[5] == 1 else 'Inactive'
            popup_text = f"<h4>{row[2]}</h4>\n\n"
            popup_text += f"<strong>Status: {status}</strong>"

            iframe = folium.Html(popup_text, script=True)

            folium.Marker(
                location=[row[3], row[4]],
                tooltip=row[2],
                popup=folium.Popup(iframe, max_width=450),
                icon=folium.Icon(icon='industry', prefix='fa', color="black" if row[5] == 1 else 'lightgray')
            ).add_to(m)

        self.truck_html = Markup(m._repr_html_().replace("`","'"))