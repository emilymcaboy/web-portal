from viewmodels.shared.viewmodelbase import ViewModelBase
from io import BytesIO
from os import listdir
from os.path import isfile, join, splitext
from flask import send_file


class EmployeeImagesViewmodel(ViewModelBase):
    def __init__(self, employeeid) -> None:
        super().__init__()

        file_path = '\\\\lsiwebsrv\\EmployeePhotos\\'
        files = [f for f in listdir(file_path) if isfile(join(file_path, f))]
        selected_file = "None.png"

        for file in files:
            file_name, _ = splitext(file)
            
            if file_name == employeeid:
                selected_file = file
        
        selected_path = join(file_path, selected_file)
        with open(selected_path, 'rb') as f:
            self.file = send_file(BytesIO(f.read()), attachment_filename=selected_file)

        
