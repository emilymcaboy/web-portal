from flask.globals import request
from flask import flash, Markup
from viewmodels.shared.viewmodelbase import ViewModelBase
from services.connections import dw, automated_tasks
from collections import OrderedDict


class BacklogViewmodel(ViewModelBase):
    def __init__(self, filter="Open") -> None:
        super().__init__()

        self.page = "Backlog"
        self.page_header = "Backlog"
        self.filter = filter
        self.filter_options = ["Open", "Recently Closed", "Hold", "All"]
    
    def get_priorities(self):
        con = automated_tasks()
        cursor = con.cursor()


        command = """
                SELECT

                PriorityID
                ,Priority

                FROM Backlog.Priority
                ORDER BY SortOrder
                """

        cursor.execute(command)
        data = cursor.fetchall()
        self.priorities = data                

    def get_project_type(self):
        con = automated_tasks()
        cursor = con.cursor()

        command = """
                SELECT
                ProjectTypeID
                ,ProjectType
                FROM AutomatedTasks.Backlog.ProjectType
                ORDER BY IIF(ProjectType = 'Other', 'zzzz', ProjectType)
                """

        cursor.execute(command)
        data = cursor.fetchall()
        self.project_types = data                

    def get_status(self):
        con = automated_tasks()
        cursor = con.cursor()

        command = """
                SELECT

                StatusID
                ,Status

                FROM Backlog.Status
                ORDER BY SortOrder
                """

        cursor.execute(command)
        data = cursor.fetchall()
        self.status = data   

    def get_requested_by(self):
        con = automated_tasks()
        cursor = con.cursor()

        command = """
                SELECT

                UserLogin
                ,EmployeeName

                FROM Backlog.vRequestedBy
                ORDER BY SortOrder DESC, EmployeeName

                """

        cursor.execute(command)
        data = cursor.fetchall()
        self.requested_by = data   

    def get_managed_by(self):
        con = automated_tasks()
        cursor = con.cursor()

        command = """
                SELECT 

                A.UserID
                ,D.Name

                FROM WebApp.Users AS A

                JOIN WebApp.GroupMembership AS B
                    ON B.UserID = A.UserID

                JOIN WebApp.Groups AS C
                    ON C.GroupID = B.GroupID

                JOIN SourceData.ActiveDirectory.Person AS D
                    ON D.UserPrincipalName = A.UserUPN

                WHERE GroupName = 'BIUser'
                """

        cursor.execute(command)
        data = cursor.fetchall()
        self.managed_by = data  

    def get_backlog(self):
        con = automated_tasks()
        cursor = con.cursor()

        command = "{ CALL Backlog.pBacklogItems(?) }"

        cursor.execute(command, [self.filter])
        data = cursor.fetchall()
        self.backlog = data


    def get_item_details(self, item_id):
        con = automated_tasks()
        cursor = con.cursor()

        command = """
                SELECT
                BacklogID
                ,ItemName
                ,ItemDescription
                ,ProjectTypeID
                ,OtherProjectTypeDescription
                ,PriorityID
                ,StatusID
                ,RequestedBy
                ,ManagedBy
                ,RequestReceived
                ,PlannedStart
                ,StartDate
                ,EndDate
                ,DueDate
                ,Notes
                FROM Backlog.Backlog
                WHERE BacklogID = ?
                """

        cursor.execute(command, [item_id])
        data = cursor.fetchone()
        self.backlog_item = {
            "backlog_id": data[0],
            "item_name": data[1],
            "item_description": data[2],
            "project_type_id": data[3],
            "other_project_description": data[4],
            "priority_id": data[5],
            "status_id": data[6],
            "requested_by": data[7],
            "managed_by": data[8],
            "request_received": data[9],
            "planned_start": data[10],
            "start_date": data[11],
            "end_date": data[12],
            "due_date": data[13],
            "notes": data[14],            
        }  

    def get_combo_data(self):
        self.get_project_type()
        self.get_priorities()
        self.get_status()
        self.get_requested_by()
        self.get_managed_by()
        self.get_backlog()


    def process_results(self):
        if "modalDeleteSubmit" in request.form.keys():
            self.delete_item()
        else:
            self.add_edit_item()

    def add_edit_item(self):
        con = automated_tasks()
        cursor = con.cursor()

        values = [
            request.form.get("submitButton"),
            request.form.get("editItemName"),
            request.form.get("editItemDescription"),
            request.form.get("editProjectType"),
            request.form.get("editOtherProjectType"),
            request.form.get("editPriority"),
            request.form.get("editStatus"),
            request.form.get("editRequestedBy"),
            request.form.get("editManagedBy"),
            request.form.get("editRequestReceived"),
            request.form.get("editPlannedStart"),
            request.form.get("editStartDate"),
            request.form.get("editEndDate"),
            request.form.get("editDueDate"),
            request.form.get("editNotes"),
        ]

        cursor.execute("{ CALL backlog.pAddEditItem(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }", values)
        cursor.commit()
        cursor.close()

    def delete_item(self):
        con = automated_tasks()
        cursor = con.cursor()

        values = [
            request.form.get("modalDeleteSubmit"),
        ]

        command = "DELETE FROM Backlog.Backlog WHERE BacklogID = ?"
        cursor.execute(command, values)
        cursor.commit()
        cursor.close()