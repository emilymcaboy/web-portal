from viewmodels.shared.viewmodelbase import ViewModelBase
from services.connections import dw


class EmployeeDirectoryViewmodel(ViewModelBase):
    def __init__(self) -> None:
        super().__init__()

        self.page = "Employee Directory"
        self.page_header = "Employee Directory"


    def get_divisions(self):
        connection = dw()
        cursor = connection.cursor()

        command = "SELECT DISTINCT DivisionDescription, DivisionAbbreviation from PR.vDirectory ORDER BY DivisionDescription"

        cursor.execute(command)

        data = cursor.fetchall()

        self.divisions = [i[0] for i in data]
        self.division_abbr = [i[1] for i in data]

    def get_employees(self):
        connection = dw()
        cursor = connection.cursor()

        command = """SELECT 

                        DivisionDescription,
                        EmployeeName,
                        JobTitle,
                        LOWER(ISNULL(Email, '')),
                        ISNULL(Phone, ''),
                        ISNULL(Cell, ''),
                        ImageURL
                        
                     FROM PR.vDirectory
                     ORDER BY EmployeeName"""

        cursor.execute(command)

        data = cursor.fetchall()

        divisions = self.divisions

        employee_by_div = {}

        for div in divisions:
            employee_by_div[div] = []

        for row in data:
            employee_by_div[row[0]].append(row)

        self.employees = employee_by_div

    
    def get_featured_employees(self):
        connection = dw()
        cursor = connection.cursor()

        command = """SELECT 

                        ISNULL(D.DivisionDescription, A.DivisionDescription),
                        EmployeeName,
                        JobTitle,
                        ImageURL
                        
                     FROM PR.vDirectory as A
                     JOIN AutomatedTasks.WebApp.DirectoryFeaturedEmployees as B
                        ON A.Employee = B.Employee
                     JOIN Global.dimDivision as D
                        ON D.Division = B.Division
                     ORDER BY B.Sequence"""

        cursor.execute(command)

        data = cursor.fetchall()

        divisions = self.divisions

        employee_by_div = {}

        for div in divisions:
            employee_by_div[div] = []

        for row in data:
            employee_by_div[row[0]].append(row)

        self.featured_employees = employee_by_div










