from viewmodels.shared.viewmodelbase import ViewModelBase
from services.data_warehouse import DataWarehouse
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col, SubmitButtons
from flask import Markup


class RunPathViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()

        self.page = "Run Path"
        self.page_header = "Run Path"
        self.message_category = "Relaod"
       
        self.dw = DataWarehouse()

        self.dw.get_paths()

        self.cards = []

        for key, value in self.dw.paths.items():
            self.cards.append(
                {
                    'header': value['path_name'],
                    'text': value['path_description'],
                    'id': key
                }
            )
        


        self.url = '/dw/runpath'

        self.action = ''

        for key, value in self.request_dict.items():
            if key.startswith('submit_'):
                self.path_id = key.replace('submit_', '')
                self.action = 'run_path'
            
