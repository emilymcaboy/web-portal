from operator import itemgetter

from flask.globals import request
from viewmodels.shared.viewmodelbase import ViewModelBase
from services.data_warehouse import DataWarehouse
from services.create_filter import DataTable, ModalForm
from flask import Markup, jsonify


class DataWarehouseUsers(ViewModelBase):
    def __init__(self):
        super().__init__()

        self.page = "DW Users"
        self.page_header = "Data Warehouse Users"
        self.message_category = "Data Warehouse"
        self.show_filters = False
        self.embed = True if self.request_dict.get('embedded', '').upper() == 'Y' else False
        self.show_header = False if self.request_dict.get('showheader', '').upper() == 'N' else True
        self.submit_buttons = []
        self.action = ''
        self.show_modal = True
        self.modal_button_text = "Create New User"
        self.modal_submit_text = "Create User"
        self.modal_title = "Create New DW User"


        self.dw = DataWarehouse()
        self.mf = ModalForm()


    def build_table(self):

        self.dw.get_users()
        self.dw.get_templates()
        self.dw.get_divisions()

        if len(self.dw.users) == 0:
            return {}

        user_list = sorted(self.dw.users.keys(), key=str.casefold)

        dt = DataTable(user_list)

        dt.create_table_col(
            col_header="Login",
            col_type="",
            col_class="align-middle",
            col_text="",
            col_data=user_list,
        )

        dt.create_table_col(
            col_header="Name",
            col_type="",
            col_class="align-middle",
            col_text="",
            col_data=[self.dw.users[user]["display_name"] for user in user_list],
        )

        dt.create_table_col(
            col_header="Title",
            col_type="",
            col_class="align-middle",
            col_text="",
            col_data=[self.dw.users[user]["title"] for user in user_list],
        )

        dt.create_table_col(
            col_header="Status",
            col_type="",
            col_class="align-middle",
            col_text="",
            col_data=[self.dw.users[user]["status"] for user in user_list],
        )

        template_options = [['', '']]
        template_options.extend([[str(i), self.dw.templates[i]['template_name']] for i in self.dw.templates.keys()])

        dt.create_table_col(
            col_header="Template",
            col_type="combo",
            col_class="align-middle",
            col_text="",
            col_default=['' if self.dw.users[user]["template_id"] is None else str(self.dw.users[user]["template_id"]) for user in user_list],
            col_data=[template_options] * len(user_list),
        )

        division_options = [['', '']]
        division_options.extend([[str(i), self.dw.divisions[i]['division'] + " - " + self.dw.divisions[i]['division_name']] for i in self.dw.divisions.keys()])

        dt.create_table_col(
            col_header="Division",
            col_type="combo",
            col_class="align-middle",
            col_text="",
            col_default=['' if self.dw.users[user]["division_id"] is None else str(self.dw.users[user]["division_id"]) for user in user_list],
            col_data=[division_options] * len(user_list),
        )

        dt.create_table_col(
            col_header="Save",
            col_type="button",
            col_class="align-middle",
            col_text="Save",
            col_data=user_list,
        )

        return dt.get_table()

    
    def build_new_user_form(self):
        
        self.mf.add_text("Login", 'LAKESIDEIND\\TestUser')

        if self.dw.templates == {}:
            self.dw.get_templates()
        if self.dw.divisions == {}:
            self.dw.get_divisions()

        division_keys = [str(i) for i in self.dw.divisions.keys()]
        division_names = [self.dw.divisions[i]['division'] + " - " + self.dw.divisions[i]['division_name'] for i in self.dw.divisions.keys()]

        self.mf.add_combo("Division",
            division_keys,
            division_names,
            None,
            )

        template_keys = [str(i) for i in self.dw.templates.keys()]
        template_names = [self.dw.templates[i]['template_name'] for i in self.dw.templates.keys()]

        self.mf.add_combo("Template",
            template_keys,
            template_names,
            None,
            )

        return self.mf.fields


    def process_results(self):

        for key, value in request.form.items():
            if key.startswith('Save_'):
                user = value
                action = 'update'
                for key, value in request.form.items():
                    if key.startswith("Template_") and key.endswith(user):
                        template = value
                    if key.startswith("Division_") and key.endswith(user):
                        division = value
            
            elif key == 'modal_submit':
                user = request.form['Login']
                template = request.form['Template']
                division = request.form['Division']
                action = 'new'
            elif key.startswith('Delete_'):
                user = value
                action = 'delete'
                template = 0
                division = 0

        self.dw.save_user(user, template, division, action)


