from viewmodels.shared.viewmodelbase import ViewModelBase
from services.data_warehouse import DataWarehouse
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col, SubmitButtons
from flask import Markup
from flask.globals import request


class SecurityTemplates(ViewModelBase):
    def __init__(self, template_id=None):
        super().__init__()

        self.page = "Security Templates"
        self.page_header = "DW Security Templates"
        self.message_category = "Data Warehouse"

        self.dw = DataWarehouse()

        self.selected_template_id = template_id

    def get_default_template(self):
        self.dw.get_templates()
        return [i for i in self.dw.templates.keys()][0]

    def get_info(self):

        self.dw.get_templates()
        self.dw.get_company_settings()
        self.dw.get_vendor_groups()
        self.dw.get_job_security_levels()

        self.templates = self.dw.templates
        self.company_settings = self.dw.company_settings
        self.vendor_groups = self.dw.vendor_groups
        self.job_security_levels = self.dw.job_security_levels

        if self.selected_template_id is None:
            self.selected_template_id = [i for i in self.templates.keys()][0]

        self.company_settings_id = self.templates[self.selected_template_id][
            "company_settings_id"
        ]
        self.vendor_group_id = self.templates[self.selected_template_id][
            "vendor_group_id"
        ]

        self.dw.get_template_schemas(self.selected_template_id)
        self.template_schemas = self.dw.template_schemas

        self.schema_access = []

        for key, value in self.template_schemas.items():
            if value[0] is not None:
                self.schema_access.append(key)

        self.dw.get_template_job_security_level(self.selected_template_id)
        self.template_job_security_levels = self.dw.template_job_security_levels

        self.dw.get_template_material_sales_level(self.selected_template_id)
        self.template_material_sales_levels = self.dw.template_material_sales_levels

        self.dw.get_template_payroll_settings(self.selected_template_id)
        self.template_payroll_settings = self.dw.template_payroll_settings

    def process_results(self):

        if "save_changes" in request.form.keys():
            template = request.form["templateCombo"]
            company_settings = request.form.get("templateCompanySettings", 2)
            vendor_group = request.form.get("templateVendorGroup", 4)

            schemas = {}
            job_security = {}
            ez_street = {}
            payroll = {}

            for key, value in request.form.items():
                if key.startswith("schema-"):
                    _, schema, co = key.split("-")
                    if schema not in schemas.keys():
                        schemas[schema] = []
                    schemas[schema].append(co)
                elif key.startswith("jobSecurity"):
                    job_security[key.replace("jobSecurity", "")] = value
                elif key.startswith("EZStreet"):
                    ez_street[key.replace("EZStreet", "")] = value
                elif key.startswith("payroll-"):
                    _, co, group, col = key.split("-")
                    if co not in payroll.keys():
                        payroll[co] = {}
                    if group not in payroll[co].keys():
                        payroll[co][group] = []
                    payroll[co][group].append(col)

            self.dw.save_template_changes(
                template,
                company_settings,
                vendor_group,
                schemas,
                job_security,
                ez_street,
                payroll,
            )

        elif "modal_create_submit" in request.form.keys():
            template = request.form["copyTemplate"]
            template_name = request.form["newTemplateName"]

            new_template_id = self.dw.create_new_template(template_name, template)

            if new_template_id is not None:
                self.selected_template_id = new_template_id

        elif "modal_delete_submit" in request.form.keys():
            template = self.selected_template_id

            success = self.dw.delete_template(template)

            if success:
                self.selected_template_id = self.get_default_template()





