from viewmodels.shared.viewmodelbase import ViewModelBase
from services.data_warehouse import DataWarehouse
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col, SubmitButtons
from flask import Markup


class RunStreamViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()

        self.page = "Run Stream"
        self.page_header = "Run Stream"
        self.message_category = "Reload"
       
        self.dw = DataWarehouse()

        self.dw.get_streams()

        self.cards = []

        for key, value in self.dw.streams.items():
            self.cards.append(
                {
                    'header': value['stream_name'],
                    'text': value['stream_description'],
                    'id': key
                }
            )
        


        self.url = '/dw/runstream'

        self.action = ''

        for key, value in self.request_dict.items():
            if key.startswith('submit_'):
                self.stream_id = key.replace('submit_', '')
                self.action = 'run_stream'
            
